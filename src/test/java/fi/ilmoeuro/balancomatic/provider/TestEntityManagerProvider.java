/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.provider;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author user
 */
@ApplicationScoped
public class TestEntityManagerProvider {

    private EntityManager entityManager;

    @PostConstruct
    public void initialize() {
        EntityManagerFactory emf = Persistence
                .createEntityManagerFactory("fi.ilmoeuro.balancomatic.testing");
        entityManager = emf.createEntityManager();
    }

    @PreDestroy
    public void close() {
        entityManager.close();
    }

    @Produces
    public EntityManager getEntityManager() {
        return entityManager;
    }
}
