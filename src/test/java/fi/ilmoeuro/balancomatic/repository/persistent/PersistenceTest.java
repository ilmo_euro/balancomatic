/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository.persistent;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.eclipse.persistence.sessions.server.ServerSession;
import org.eclipse.persistence.tools.schemaframework.SchemaManager;
import org.junit.After;

/**
 *
 * @author user
 */
public abstract class PersistenceTest {

    private EntityTransaction entityTransaction;

    @Inject
    protected EntityManager entityManager;

    @After
    public void tearDown() {
        ServerSession session = entityManager.unwrap(ServerSession.class);
        SchemaManager schemaManager = new SchemaManager(session);
        schemaManager.replaceDefaultTables(true, true);
    }

    protected void begin() {
        entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
    }

    protected void end() {
        entityManager.flush();
        entityTransaction.commit();
        entityTransaction = null;
    }
}
