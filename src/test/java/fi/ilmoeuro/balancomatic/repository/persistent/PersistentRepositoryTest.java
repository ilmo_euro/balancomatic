/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository.persistent;

import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.provider.TestEntityManagerProvider;
import java.util.Collection;
import javax.inject.Inject;
import javax.persistence.EntityTransaction;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.jglue.cdiunit.DummyHttpRequest;
import org.jglue.cdiunit.InRequestScope;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author user
 */
@RunWith(CdiRunner.class)
@AdditionalClasses({
    PersistentProjects.class,
    TestEntityManagerProvider.class,
    DummyHttpRequest.class,})
public class PersistentRepositoryTest extends PersistenceTest {

    @Inject
    private PersistentProjects projects;

    @Test
    @InRequestScope
    public void testCreate() {
        EntityTransaction etx = entityManager.getTransaction();
        etx.begin();
        Project project = projects.create();
        project.setName("test name");
        etx.commit();
    }

    @Test
    @InRequestScope
    public void testFindById() {
        EntityTransaction etx = entityManager.getTransaction();
        etx.begin();
        Project project = projects.create();
        project.setName("test name");
        etx.commit();

        etx.begin();
        Collection<Project> p = projects.listAll();
        Project proj = p.iterator().next();
        Long id = proj.getId();
        etx.commit();

        etx.begin();
        proj = projects.findById(id);
        assertEquals("test name", proj.getName());
        etx.commit();
    }

    @Test
    @InRequestScope
    public void testListAll() {
        EntityTransaction etx = entityManager.getTransaction();
        etx.begin();
        Project project = projects.create();
        project.setName("test name");
        etx.commit();

        etx.begin();
        Collection<Project> p = projects.listAll();
        Project proj = p.iterator().next();
        assertEquals("test name", proj.getName());
        etx.commit();
    }

    @Test
    @InRequestScope
    public void testDelete() {
        EntityTransaction etx = entityManager.getTransaction();
        etx.begin();
        Project project = projects.create();
        project.setName("test name");
        etx.commit();

        etx.begin();
        Collection<Project> p = projects.listAll();
        Project proj = p.iterator().next();
        projects.delete(proj);
        etx.commit();

        etx.begin();
        p = projects.listAll();
        assertTrue(p.isEmpty());
        etx.commit();
    }
}
