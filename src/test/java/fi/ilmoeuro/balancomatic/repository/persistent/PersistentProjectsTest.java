/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository.persistent;

import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.provider.TestEntityManagerProvider;
import fi.ilmoeuro.balancomatic.repository.Projects;
import javax.inject.Inject;
import javax.persistence.RollbackException;
import javax.validation.ConstraintViolationException;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.jglue.cdiunit.DummyHttpRequest;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;

/**
 *
 * @author user
 */
@RunWith(CdiRunner.class)
@AdditionalClasses({
    PersistentProjects.class,
    TestEntityManagerProvider.class,
    DummyHttpRequest.class,})
public class PersistentProjectsTest extends PersistenceTest {

    @Inject
    Projects projects;

    @Test
    public void testInvalidName() {
        begin();
        Project project = projects.create();
        project.setName("//");
        try {
            end();
        } catch (RollbackException ex) {
            assertTrue(ex.getCause() instanceof ConstraintViolationException);
            return; // pass
        }
        fail("Invalid entity passed validation");
    }

    @Test
    public void testFindByContest() {
    }

}
