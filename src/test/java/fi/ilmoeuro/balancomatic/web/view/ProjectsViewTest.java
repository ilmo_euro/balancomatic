/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.view;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.repository.Projects;
import fi.ilmoeuro.balancomatic.repository.transient_.TransientProjects;
import fi.ilmoeuro.balancomatic.web.context.CurrentEntitiesContext;
import fi.ilmoeuro.balancomatic.web.view.ProjectsView.ProjectView;
import java.util.Collection;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.UserTransaction;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.jglue.cdiunit.InRequestScope;
import org.jglue.cdiunit.ProducesAlternative;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Student
 */
@RunWith(CdiRunner.class)
@AdditionalClasses({
    Projects.class,
    ProjectsView.class,
    HttpServletRequest.class,
    UserTransaction.class,})
public class ProjectsViewTest {

    private Projects projects;
    @Produces
    @Mock
    private HttpServletRequest httpServletRequest;
    @Produces
    @Alternative
    @ProducesAlternative
    @Mock
    private CurrentEntitiesContext projectContext;
    @Inject
    private ProjectsView projectsView;

    @Produces
    public Projects getProjects() {
        return projects;
    }

    @Before
    public void setUp() {
        projects = new TransientProjects();
    }

    @After
    public void tearDown() {
        projects = null;
    }

    @Test
    @InRequestScope
    public void testGetAllProjects() {
        final Project project1 = projects.create();
        final Project project2 = projects.create();
        project1.setName("project1");
        project2.setName("project2");
        Collection<ProjectView> projectViews;
        projectViews = projectsView.getAllProjects();
        assertTrue(Iterables.any(projectViews,
                new Predicate<ProjectView>() {
                    @Override
                    public boolean apply(ProjectView t) {
                        return t.getId().equals(project1.getId());
                    }
                }));
        assertTrue(Iterables.any(projectViews,
                new Predicate<ProjectView>() {
                    @Override
                    public boolean apply(ProjectView t) {
                        return t.getId().equals(project2.getId());
                    }
                }));
        assertEquals(2, projectViews.size());
    }

    @Test
    @InRequestScope
    public void testCurrentId() {
        final Project project1 = projects.create();
        final Project project2 = projects.create();
        project1.setName("project1");
        project2.setName("project2");
        when(projectContext.getProjectId()).thenReturn(project1.getId());
        Collection<ProjectView> projectViews;
        projectViews = projectsView.getAllProjects();
        assertTrue(Iterables.any(projectViews,
                new Predicate<ProjectView>() {
                    @Override
                    public boolean apply(ProjectView t) {
                        if (t.isCurrent()) {
                            return t.getId().equals(project1.getId());
                        } else {
                            return false;
                        }
                    }
                }));
    }
}
