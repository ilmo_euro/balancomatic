/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.servlet;

import fi.ilmoeuro.balancomatic.web.aop.HtmlEscapeFilter;
import fi.ilmoeuro.balancomatic.web.context.ErrorContext;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import java.util.concurrent.atomic.AtomicReference;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.jglue.cdiunit.ContextController;
import org.jglue.cdiunit.ProducesAlternative;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

/**
 *
 * @author user
 */
@RunWith(CdiRunner.class)
@AdditionalClasses({
    HttpServletRequest.class,
    UserTransaction.class,})
public class HtmlEscapeFilterTest {

    @Inject
    private ViewContext viewContext;
    @Inject
    private ErrorContext errorContext;
    @Inject
    private ContextController contextController;
    private RequestDispatcher requestDispatcher;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private FilterChain filterChain;

    @Before
    public void setUp() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        filterChain = mock(FilterChain.class);
        requestDispatcher = mock(RequestDispatcher.class);
        when(request.getContextPath()).thenReturn("/test");
        when(request.getRequestDispatcher(anyString()))
                .thenReturn(requestDispatcher);
    }

    @Produces
    @ProducesAlternative
    public HttpServletRequest getHttpServletRequest() {
        return request;
    }

    @Produces
    @ProducesAlternative
    public ServletRequest getServletRequest() {
        return request;
    }

    @Test
    public void testEscape() throws Exception {
        HtmlEscapeFilter instance = new HtmlEscapeFilter();
        final AtomicReference<Boolean> answered = new AtomicReference<>(false);

        when(request.getParameter(anyString())).thenReturn("<script></script>");
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ServletRequest req = (ServletRequest) args[0];
                assertEquals("&lt;script&gt;&lt;/script&gt;",
                        req.getParameter("test"));
                answered.set(true);
                return null;
            }
        }).when(filterChain).doFilter(any(ServletRequest.class),
                any(ServletResponse.class));

        instance.doFilter(request, response, filterChain);

        assertTrue(answered.get());
    }

    @Test
    public void testNoEscape() throws Exception {
        HtmlEscapeFilter instance = new HtmlEscapeFilter();
        final AtomicReference<Boolean> answered = new AtomicReference<>(false);

        when(request.getParameter(anyString())).thenReturn("Innocent string");
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                ServletRequest req = (ServletRequest) args[0];
                assertEquals("Innocent string",
                        req.getParameter("test"));
                answered.set(true);
                return null;
            }
        }).when(filterChain).doFilter(any(ServletRequest.class),
                any(ServletResponse.class));

        instance.doFilter(request, response, filterChain);

        assertTrue(answered.get());
    }
}
