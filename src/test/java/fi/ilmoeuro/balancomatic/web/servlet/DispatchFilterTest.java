/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.servlet;

import fi.ilmoeuro.balancomatic.web.aop.DispatchFilter;
import fi.ilmoeuro.balancomatic.web.context.ErrorContext;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import java.io.IOException;
import java.util.ArrayList;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.jglue.cdiunit.ContextController;
import org.jglue.cdiunit.InRequestScope;
import org.jglue.cdiunit.ProducesAlternative;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

/**
 *
 * @author Student
 */
@RunWith(CdiRunner.class)
@AdditionalClasses({
    ErrorContext.class,
    HttpServletRequest.class,})
public class DispatchFilterTest {

    @Inject
    private ViewContext viewContext;
    @Inject
    private ErrorContext errorContext;
    @Inject
    private DispatchFilter dispatchFilter;
    @Inject
    private ContextController contextController;
    private RequestDispatcher requestDispatcher;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private FilterChain filterChain;

    @Before
    public void setUp() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        filterChain = mock(FilterChain.class);
        requestDispatcher = mock(RequestDispatcher.class);
        when(request.getContextPath()).thenReturn("/test");
        when(request.getRequestDispatcher(anyString()))
                .thenReturn(requestDispatcher);
    }

    @After
    public void tearDown() {
    }

    @Produces
    @ProducesAlternative
    public HttpServletRequest getHttpServletRequest() {
        return request;
    }

    @Produces
    @ProducesAlternative
    public ServletRequest getServletRequest() {
        return request;
    }

    @Test
    @InRequestScope
    public void testNullDispatch() throws IOException, ServletException {
        dispatchFilter.doFilter(request, response, filterChain);
        verify(request, never()).getRequestDispatcher(any(String.class));
        verify(response, never()).sendRedirect(anyString());
    }

    @Test
    @InRequestScope
    public void testForwardDispatch() throws ServletException, IOException {
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                viewContext.setView("test");
                return null;
            }
        }).when(filterChain).doFilter(request, response);
        dispatchFilter.doFilter(request, response, filterChain);
        verify(request)
                .getRequestDispatcher("/WEB-INF/views/test.jsp");
        verify(requestDispatcher)
                .forward(request, response);
        verify(response, never()).sendRedirect(anyString());
    }

    @Test
    @InRequestScope
    public void testRedirectDispatch() throws ServletException, IOException {
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                viewContext.setRedirect("/test/");
                return null;
            }
        }).when(filterChain).doFilter(request, response);

        dispatchFilter.doFilter(request, response, filterChain);
        verify(request, never()).getRequestDispatcher(any(String.class));
        verify(response).sendRedirect("/test/");
    }

    @Test
    @InRequestScope
    public void testErrorDispatch() throws ServletException, IOException {
        errorContext.setErrorMessage("Test error");
        errorContext.setDetails(new ArrayList<String>());
        errorContext.setLastPage("/test/test/");
        when(request.getSession(anyBoolean()))
                .thenReturn(mock(HttpSession.class));
        dispatchFilter.doFilter(request, response, filterChain);
        verify(request)
                .getRequestDispatcher("/WEB-INF/views/error.jsp");
        verify(requestDispatcher)
                .forward(request, response);
        verify(response, never()).sendRedirect(anyString());
    }
}
