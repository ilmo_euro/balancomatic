/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author user
 */
@Entity
@NamedQueries({
    @NamedQuery(
            name = "CostCalculation.listAll",
            query = "SELECT c FROM CostCalculation c"),
    @NamedQuery(
            name = "CostCalculation.listByProjectId",
            query = "SELECT c FROM CostCalculation c"
            + " INNER JOIN c.project p"
            + " WHERE p.id = :projectId"),
    @NamedQuery(
            name = "CostCalculation.findById",
            query = "SELECT c FROM CostCalculation c WHERE c.id = :id"),})
public class CostCalculation
        implements Serializable, fi.ilmoeuro.balancomatic.repository.Entity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    @NotNull
    private Long numContests;
    @ManyToOne
    @NotNull
    private Project project;
    @NotNull
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date date;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CostCalculation)) {
            return false;
        }
        CostCalculation other = (CostCalculation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fi.ilmoeuro.balancomatic.data.CostCalculationSnapshot[ id=" + id + " ]";
    }

    /**
     * @return the number of contests
     */
    public Long getNumContests() {
        return numContests;
    }

    /**
     * @param numContests the number of contests
     */
    public void setNumContests(Long numContests) {
        this.numContests = numContests;
    }

    /**
     * @return the project
     */
    public Project getProject() {
        return project;
    }

    /**
     * @param project the project to set
     */
    public void setProject(Project project) {
        this.project = project;
    }

    /**
     * @return the calculation date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the calculation date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }
}
