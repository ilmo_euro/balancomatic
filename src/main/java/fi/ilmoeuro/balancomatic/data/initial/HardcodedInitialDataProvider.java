/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.data.initial;

import fi.ilmoeuro.balancomatic.config.ConfigProperty;
import fi.ilmoeuro.balancomatic.data.Account;
import fi.ilmoeuro.balancomatic.data.Project;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

/**
 *
 * @author user
 */
@Dependent
public class HardcodedInitialDataProvider implements InitialDataProvider {

    @Inject
    @ConfigProperty("security.salt")
    private String salt;

    @Override
    public List<Object> provideEntities() {
        List<Object> result = new ArrayList<>();
        Project project = new Project();
        project.setName("Example Project");
        result.add(project);
        project = new Project();
        project.setName("Another Example Project");
        result.add(project);
        Account account = new Account();
        account.setName("admin");
        account.setPassword("admin", salt);
        result.add(account);
        return result;
    }
}
