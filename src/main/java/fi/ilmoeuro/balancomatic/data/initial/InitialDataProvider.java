/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.data.initial;

import java.util.List;

public interface InitialDataProvider {

    public List<Object> provideEntities();
}
