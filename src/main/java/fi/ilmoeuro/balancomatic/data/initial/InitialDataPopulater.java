/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.data.initial;

import fi.ilmoeuro.balancomatic.data.Property;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.transaction.UserTransaction;

@WebListener
public class InitialDataPopulater implements ServletContextListener {

    @Inject
    private InitialDataProvider provider;
    @Inject
    private EntityManager entityManager;
    @Resource
    private UserTransaction utx;

    private boolean isAlreadyPopulated() {
        Property property;
        try {
            property = entityManager
                    .createNamedQuery("Property.findById", Property.class)
                    .setParameter("id", "hasInitialData")
                    .getSingleResult();
            if ("true".equals(property.getValue())) {
                return true;
            } else {
                return false;
            }
        } catch (NoResultException ex) {
            return false;
        }
    }

    private void setAlreadyPopulated(boolean value) {
        Property property;
        try {
            property = entityManager
                    .createNamedQuery("Property.findById", Property.class)
                    .setParameter("id", "hasInitialData")
                    .getSingleResult();
        } catch (NoResultException ex) {
            property = new Property();
            property.setId("hasInitialData");
        }
        property.setValue(value ? "true" : "false");
        entityManager.persist(property);
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            utx.begin();
            entityManager.joinTransaction();
            if (isAlreadyPopulated()) {
                utx.commit();
                return;
            }
            List<Object> entities = provider.provideEntities();
            for (Object entity : entities) {
                entityManager.persist(entity);
            }
            setAlreadyPopulated(true);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception ex1) {
                Logger.getLogger(InitialDataPopulater.class.getName())
                        .log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(InitialDataPopulater.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
