/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.Pattern;

/**
 *
 * @author user
 */
@Entity
@NamedQueries({
    @NamedQuery(
            name = "Project.listAll",
            query = "SELECT p FROM Project p"),
    @NamedQuery(
            name = "Project.findById",
            query = "SELECT p FROM Project p WHERE p.id = :id"),
    @NamedQuery(
            name = "Project.findByContestId",
            query = "SELECT DISTINCT p FROM Resource r"
            + " INNER JOIN r.resourceType rt"
            + " INNER JOIN rt.project p"
            + " INNER JOIN r.contest c"
            + " WHERE c.id = :contestId"),})
public class Project
        implements Serializable, fi.ilmoeuro.balancomatic.repository.Entity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    @Pattern(regexp = "\\w(\\w|\\s)*",
            message = "Invalid characters in project name")
    private String name;

    /**
     * @return the project name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Project)) {
            return false;
        }
        Project other = (Project) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fi.ilmoeuro.balancomatic.data.Project[ id=" + id + " ]";
    }
}
