/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

/**
 *
 * @author user
 */
@Entity
@NamedQueries({
    @NamedQuery(
            name = "ResourceTypeCostCalculation.listAll",
            query = "SELECT c FROM ResourceTypeCostCalculation c"),
    @NamedQuery(
            name = "ResourceTypeCostCalculation.findById",
            query = "SELECT c FROM ResourceTypeCostCalculation c"
            + " WHERE c.id = :id"),
    @NamedQuery(
            name = "ResourceTypeCostCalculation."
            + "findByResourceTypeIdAndCostCalculationId",
            query = "SELECT c FROM ResourceTypeCostCalculation c"
            + " INNER JOIN c.resourceType rt"
            + " INNER JOIN c.snapshot cc"
            + " WHERE rt.id = :resourceTypeId AND cc.id = :costCalculationId"
    )})
public class ResourceTypeCostCalculation implements Serializable,
        fi.ilmoeuro.balancomatic.repository.Entity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @NotNull
    private ResourceType resourceType;
    @ManyToOne
    @NotNull
    private CostCalculation snapshot;
    @Column
    @NotNull
    private Double optimalCost;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ResourceTypeCostCalculation)) {
            return false;
        }
        ResourceTypeCostCalculation other = (ResourceTypeCostCalculation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fi.ilmoeuro.balancomatic.data.ResourceTypeCostCalculation[ id=" + id + " ]";
    }

    /**
     * @return the resourceType
     */
    public ResourceType getResourceType() {
        return resourceType;
    }

    /**
     * @param resourceType the resourceType to set
     */
    public void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    /**
     * @return the snapshot
     */
    public CostCalculation getSnapshot() {
        return snapshot;
    }

    /**
     * @param snapshot the snapshot to set
     */
    public void setSnapshot(CostCalculation snapshot) {
        this.snapshot = snapshot;
    }

    /**
     * @return the optimalCost
     */
    public Double getOptimalCost() {
        return optimalCost;
    }

    /**
     * @param optimalCost the optimalCost to set
     */
    public void setOptimalCost(Double optimalCost) {
        this.optimalCost = optimalCost;
    }

}
