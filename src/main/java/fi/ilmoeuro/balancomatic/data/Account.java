/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.data;

import java.io.Serializable;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.apache.commons.codec.binary.Hex;

/**
 *
 * @author user
 */
@Entity
@NamedQueries({
    @NamedQuery(
            name = "Account.listAll",
            query = "SELECT u FROM Account u"),
    @NamedQuery(
            name = "Account.findById",
            query = "SELECT u FROM Account u WHERE u.id = :id"),
    @NamedQuery(
            name = "Account.findByName",
            query = "SELECT u FROM Account u WHERE u.name = :name"),})
public class Account
        implements Serializable, fi.ilmoeuro.balancomatic.repository.Entity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique = true)
    @Pattern(regexp = "\\w(\\w|\\s)*",
            message = "Invalid characters in user name")
    private String name;
    @Column
    @NotNull
    @Size(min = 1)
    private String hashedPassword;

    /**
     * @return the project name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Account)) {
            return false;
        }
        Account other = (Account) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fi.ilmoeuro.balancomatic.data.Project[ id=" + id + " ]";
    }

    /**
     * @return the hashed password
     */
    public String getHashedPassword() {
        return hashedPassword;
    }

    /**
     * @param hashedPassword the hashed password to set
     */
    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    private String hash(String candidate, String salt) {
        String hashed;
        try {
            SecretKeyFactory skf = SecretKeyFactory
                    .getInstance("PBKDF2WithHmacSHA1");
            KeySpec ks = new PBEKeySpec(candidate.toCharArray(),
                    salt.getBytes(), 1024, 128);
            SecretKey sk = skf.generateSecret(ks);
            Key k = new SecretKeySpec(sk.getEncoded(), "AES");
            hashed = Hex.encodeHexString(k.getEncoded());

        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE,
                    null, ex);
            hashed = null;
        }
        return hashed;
    }

    /**
     * Check if a given password matches this accounts password
     *
     * @param candidate The password from user input.
     * @return true if the candidate matches.
     */
    public boolean passwordMatches(String candidate, String salt) {
        return hashedPassword.equals(hash(candidate, salt));
    }

    /**
     * Hash the given <b>unhashed</b> password and set it as this accounts
     * password.
     *
     * @param unhashedPassword The target password of this account.
     */
    public void setPassword(String unhashedPassword, String salt) {
        setHashedPassword(hash(unhashedPassword, salt));
    }
}
