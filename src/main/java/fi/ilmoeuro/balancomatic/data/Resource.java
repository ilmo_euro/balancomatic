/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Student
 */
@Table(uniqueConstraints = @UniqueConstraint(
        columnNames = {"resourceType_id", "contest_id"}))
@Entity
@NamedQueries({
    @NamedQuery(
            name = "Resource.listAll",
            query = "SELECT r FROM Resource r"),
    @NamedQuery(
            name = "Resource.findById",
            query = "SELECT r FROM Resource r WHERE r.id = :id"),
    @NamedQuery(
            name = "Resource.listByContestId",
            query = "SELECT r FROM Resource r"
            + " JOIN r.contest c" + ""
            + " WHERE c.id = :contestId"),
    @NamedQuery(
            name = "Resource.findByContestIdAndResourceTypeId",
            query = "SELECT r FROM Resource r"
            + " INNER JOIN r.contest c"
            + " INNER JOIN r.resourceType rt"
            + " WHERE c.id = :contestId AND rt.id = :resourceTypeId"),
    @NamedQuery(
            name = "Resource.listByProjectId",
            query = "SELECT r FROM Resource r"
            + " INNER JOIN r.resourceType rt"
            + " INNER JOIN rt.project p"
            + " WHERE p.id = :projectId"),})
public class Resource
        implements Serializable, fi.ilmoeuro.balancomatic.repository.Entity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @NotNull
    private Contest contest;
    @ManyToOne
    @NotNull
    private ResourceType resourceType;
    @Column
    @NotNull
    private Double value_;

    /**
     * @return the value_
     */
    public Double getValue() {
        return value_;
    }

    /**
     * @param value the value_ to set
     */
    public void setValue(Double value) {
        this.value_ = value;
    }

    /**
     * @return the contest
     */
    public Contest getContest() {
        return contest;
    }

    /**
     * @param contest the contest to set
     */
    public void setContest(Contest contest) {
        this.contest = contest;
    }

    /**
     * @return the type of the resource.
     */
    public ResourceType getResourceType() {
        return resourceType;
    }

    /**
     * @param resourceType the resource type to set.
     */
    public void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Resource)) {
            return false;
        }
        Resource other = (Resource) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fi.ilmoeuro.balancomatic.data.Resource[ id=" + id + " ]";
    }
}
