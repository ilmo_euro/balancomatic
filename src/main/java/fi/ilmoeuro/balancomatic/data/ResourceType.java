/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author Student
 */
@Entity
@NamedQueries({
    @NamedQuery(
            name = "ResourceType.listAll",
            query = "SELECT r FROM ResourceType r"),
    @NamedQuery(
            name = "ResourceType.findById",
            query = "SELECT r FROM ResourceType r WHERE r.id = :id"),
    @NamedQuery(
            name = "ResourceType.listByProjectId",
            query = "SELECT rt FROM ResourceType rt"
            + " INNER JOIN rt.project p"
            + " WHERE p.id = :projectId"),
    @NamedQuery(
            name = "ResourceType.listByProjectIdOrderByTypeId",
            query = "SELECT rt FROM ResourceType rt"
            + " INNER JOIN rt.project p"
            + " WHERE p.id = :projectId"
            + " ORDER BY rt.typeId"),
    @NamedQuery(
            name = "ResourceType.findByProjectIdAndTypeId",
            query = "SELECT rt FROM ResourceType rt"
            + " INNER JOIN rt.project p"
            + " WHERE p.id = :projectId"
            + " AND rt.typeId = :typeId"),
    @NamedQuery(
            name = "ResourceType.listByContestId",
            query = "SELECT DISTINCT rt FROM Resource r"
            + " INNER JOIN r.resourceType rt"
            + " INNER JOIN r.contest c"
            + " WHERE c.id = :contestId"),})
public class ResourceType
        implements Serializable, fi.ilmoeuro.balancomatic.repository.Entity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    @NotNull
    @Pattern(regexp = "\\w+", message = "TypeID must be alphanumeric.")
    private String typeId;
    @ManyToOne
    @NotNull
    private Project project;
    @Column
    @NotNull
    @Size(min = 1, message = "Name must not be empty")
    private String name;
    @Column
    @NotNull
    private Double minValue;
    @Column
    @NotNull
    private Double maxValue;

    /**
     * @return the name of the resource.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name of the resource.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the minimum allowed value of the resource.
     */
    public Double getMinValue() {
        return minValue;
    }

    /**
     * @param minValue the minimum allowed value of the resource.
     */
    public void setMinValue(Double minValue) {
        this.minValue = minValue;
    }

    /**
     * @return the the maximum allowed value of the resource.
     */
    public Double getMaxValue() {
        return maxValue;
    }

    /**
     * @param maxValue the the maximum allowed value of the resource.
     */
    public void setMaxValue(Double maxValue) {
        this.maxValue = maxValue;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ResourceType)) {
            return false;
        }
        ResourceType other = (ResourceType) object;
        return (this.id != null || other.id == null)
                && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "fi.ilmoeuro.balancomatic.data.ResourceType[ id=" + id + " ]";
    }

    /**
     * @return the project
     */
    public Project getProject() {
        return project;
    }

    /**
     * @param project the project to set
     */
    public void setProject(Project project) {
        this.project = project;
    }

    /**
     * @return the typeId
     */
    public String getTypeId() {
        return typeId;
    }

    /**
     * @param typeId the typeId to set
     */
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
}
