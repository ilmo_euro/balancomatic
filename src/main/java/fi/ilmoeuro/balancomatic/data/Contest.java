/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author Student
 */
@Entity
@NamedQueries({
    @NamedQuery(
            name = "Contest.listAll",
            query = "SELECT c FROM Contest c"),
    @NamedQuery(
            name = "Contest.findById",
            query = "SELECT c FROM Contest c WHERE c.id = :id"),
    @NamedQuery(
            name = "Contest.listByProjectId",
            query = "SELECT DISTINCT c FROM Resource r"
            + " INNER JOIN r.resourceType rt"
            + " INNER JOIN r.contest c"
            + " INNER JOIN rt.project p"
            + " WHERE p.id = :projectId"),})
public class Contest
        implements Serializable, fi.ilmoeuro.balancomatic.repository.Entity {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private Boolean outcome;

    /**
     * @return the outcome of the contest: True for victory, False for
     * non-victory.
     */
    public Boolean getOutcome() {
        return outcome;
    }

    /**
     * @param outcome the outcome of the contest: True for victory, False for
     * non-victory.
     */
    public void setOutcome(Boolean outcome) {
        this.outcome = outcome;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Contest)) {
            return false;
        }
        Contest other = (Contest) object;
        return (this.id != null || other.id == null)
                && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "fi.ilmoeuro.balancomatic.data.Contest[ id=" + id + " ]";
    }
}
