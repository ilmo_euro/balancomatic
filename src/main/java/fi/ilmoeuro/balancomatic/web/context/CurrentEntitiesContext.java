/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.context;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author user
 */
@Named("current")
@RequestScoped
public class CurrentEntitiesContext {

    private Long projectId;
    private Long resourceTypeId;
    private Long contestId;

    /**
     * @return the id of the currently displayed project
     */
    public Long getProjectId() {
        return projectId;
    }

    /**
     * @param projectId set this to choose the currently displayed project
     */
    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public boolean isProjectDisplayed() {
        return projectId != null;
    }

    /**
     * @return the resourceTypeId
     */
    public Long getResourceTypeId() {
        return resourceTypeId;
    }

    /**
     * @param resourceTypeId the resourceTypeId to set
     */
    public void setResourceTypeId(Long resourceTypeId) {
        this.resourceTypeId = resourceTypeId;
    }

    /**
     * @return the contestId
     */
    public Long getContestId() {
        return contestId;
    }

    /**
     * @param contestId the contestId to set
     */
    public void setContestId(Long contestId) {
        this.contestId = contestId;
    }

}
