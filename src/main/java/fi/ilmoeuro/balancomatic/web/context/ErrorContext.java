/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author user
 */
@RequestScoped
@Named
public class ErrorContext implements Serializable {

    private String lastPage;
    private Collection<String> details;
    private String errorMessage;

    /**
     * @return the URI of the last successful page view (not POST action)
     */
    public String getLastPage() {
        return lastPage;
    }

    /**
     * @param lastPage the URI of the last successful page view (not POST
     * action)
     */
    public void setLastPage(String lastPage) {
        this.lastPage = lastPage;
    }

    /**
     * @return the constraint violations
     */
    public Collection<String> getDetails() {
        if (details != null) {
            return Collections.unmodifiableCollection(details);
        } else {
            return Collections.unmodifiableCollection(new ArrayList<String>());
        }
    }

    /**
     * @param details Detailed analysis of errors
     */
    public void setDetails(Collection<String> details) {
        this.details = new ArrayList<>(details);
    }

    /**
     * @return the error message
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage the error message
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return true if an error is present.
     */
    public boolean isError() {
        return errorMessage != null;
    }

    public void clear() {
        details = null;
        errorMessage = null;
    }
}
