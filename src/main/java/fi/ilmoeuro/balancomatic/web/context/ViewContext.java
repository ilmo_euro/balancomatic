/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.context;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author user
 */
@Named("context")
@RequestScoped
public class ViewContext {
    private String currentView;
    private String currentSubView;
    private String currentRedirect;
    private String contextPath;

    /**
     * @return the ID of the current view
     */
    public String getView() {
        return currentView;
    }

    /**
     * @param currentView set this to choose the current view
     */
    public void setView(String currentView) {
        if (getRedirect() != null) {
            throw new IllegalStateException(
                    "Redirect and view are mutually exclusive");
        }
        this.currentView = currentView;
    }

    /**
     * @return the current context path. Supplied by request.getContextPath().
     * Purely for convenience.
     *
     */
    public String getPath() {
        return contextPath;
    }

    /**
     * @param path the current context path. Supply this with
     * request.getContextPath().
     */
    public void setPath(String path) {
        this.contextPath = path;
    }

    /**
     * @return the path to redirect to
     */
    public String getRedirect() {
        return currentRedirect;
    }

    /**
     * @param currentRedirect set this to redirect to a path
     */
    public void setRedirect(String currentRedirect) {
        if (getView() != null) {
            throw new IllegalStateException(
                    "Redirect and view are mutually exclusive");
        }
        this.currentRedirect = currentRedirect;
    }

    /**
     * @return the current sub view. Some views have sub views that can be
     * optionally shown.
     */
    public String getSubView() {
        return currentSubView;
    }

    /**
     * @param currentSubView the current sub view.
     */
    public void setSubView(String currentSubView) {
        this.currentSubView = currentSubView;
    }
}
