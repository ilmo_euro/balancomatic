/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.context;

import fi.ilmoeuro.balancomatic.data.Account;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author user
 */
@SessionScoped
@Named("auth")
public class AuthContext implements Serializable {

    private Account loggedInAccount;

    public boolean isLoggedIn() {
        return getLoggedInAccount() != null;
    }

    /**
     * @return the loggedInAccount
     */
    public Account getLoggedInAccount() {
        return loggedInAccount;
    }

    /**
     * @param loggedInAccount the loggedInAccount to set
     */
    public void setLoggedInAccount(Account loggedInAccount) {
        this.loggedInAccount = loggedInAccount;
    }
}
