/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.aop;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import fi.ilmoeuro.balancomatic.web.context.ErrorContext;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import fi.ilmoeuro.balancomatic.web.error.TransactionException;
import fi.ilmoeuro.balancomatic.web.error.ValidationException;
import java.io.IOException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.Collection;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 *
 * @author user
 */
@WebFilter(
        urlPatterns = {"/*"},
        filterName = "errorFilter")
public class ErrorFilter implements Filter {

    @Inject
    private ViewContext viewContext;
    @Inject
    private ErrorContext errorContext;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    private void constraintViolations(Collection<ConstraintViolation<?>> cv) {
        // TODO: configurable messages
        errorContext.setErrorMessage("Constraint violations:");
        errorContext.setDetails(
                Collections2.transform(cv,
                        new Function<ConstraintViolation<?>, String>() {
                            @Override
                            public String apply(ConstraintViolation<?> input) {
                                return input.getMessage();
                            }
                        }));
        viewContext.setRedirect(null);
        viewContext.setView(null);
    }

    @Override
    public void doFilter(ServletRequest request,
            ServletResponse response,
            FilterChain chain)
            throws IOException,
            ServletException {
        HttpServletRequest hRequest = (HttpServletRequest) request;
        HttpServletResponse hResponse = (HttpServletResponse) response;
        if (hRequest.getSession(false) != null) {
            try {
                chain.doFilter(request, response);
                errorContext.clear();
                if (!"/static".equals(hRequest.getServletPath())) {
                    errorContext.setLastPage(hRequest.getServletPath()
                            + hRequest.getPathInfo()
                            + hRequest.getQueryString());
                }
            } catch (ValidationException cex) {
                constraintViolations(cex.getViolations());
            } catch (TransactionException ex) {
                if (ExceptionUtils.indexOfType(ex,
                        SQLIntegrityConstraintViolationException.class) != -1) {
                    errorContext.setErrorMessage("Deleting this entity would"
                            + " leave orphaned entities");
                } else {
                    errorContext.setErrorMessage("Error: " + ex.toString());
                }
                errorContext.setDetails(new ArrayList<String>());
                viewContext.setRedirect(null);
                viewContext.setView(null);
            } catch (Exception ex) {
                ex.printStackTrace();
                errorContext.setErrorMessage("Error: " + ex.toString());
                errorContext.setDetails(new ArrayList<String>());
                viewContext.setRedirect(null);
                viewContext.setView(null);
            }
        } else {
            try {
                chain.doFilter(request, response);
            } catch (Exception ex) {
                hResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                        ex.getMessage());
            }
        }
    }

    @Override
    public void destroy() {
    }
}
