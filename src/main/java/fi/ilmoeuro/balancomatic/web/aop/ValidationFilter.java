/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.aop;

import fi.ilmoeuro.balancomatic.web.error.ValidationException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.eclipse.persistence.internal.sessions.UnitOfWorkImpl;

/**
 *
 * @author user
 */
@WebFilter(filterName = "validationFilter", urlPatterns = {"/*"})
public class ValidationFilter implements Filter {

    @Inject
    private EntityManager entityManager;

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        chain.doFilter(request, response);

        Set<Object> objects = entityManager
                .unwrap(UnitOfWorkImpl.class)
                .getCloneMapping()
                .keySet();

        ValidatorFactory validatorFactory = Validation
                .buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<?>> violations = new HashSet<>();
        for (Object o : objects) {
            violations.addAll(
                    validator.validate(o, javax.validation.groups.Default.class));
            Logger.getLogger(ValidationFilter.class.getName())
                    .log(Level.INFO, "Object in transaction: " + o);
        }

        if (violations.size() > 0) {
            throw new ValidationException(violations);
        }
    }

    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    @Override
    public void init(FilterConfig filterConfig) {
    }
}
