/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.aop;

import fi.ilmoeuro.balancomatic.web.context.ErrorContext;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
@WebFilter(filterName = "dispatchFilter", urlPatterns = {"/*"})
public class DispatchFilter implements Filter {

    @Inject
    private ViewContext viewContext;
    @Inject
    private ErrorContext errorContext;

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest hRequest = (HttpServletRequest) request;
        HttpServletResponse hResponse = (HttpServletResponse) response;
        viewContext.setPath(hRequest.getContextPath());

        chain.doFilter(request, response);

        if (viewContext.getView() != null) {
            hRequest.getRequestDispatcher("/WEB-INF/views/"
                    + viewContext.getView()
                    + ".jsp")
                    .forward(request, response);
        } else if (viewContext.getRedirect() != null) {
            // TODO: 303 / 307
            hResponse.sendRedirect(viewContext.getRedirect());
        } else if (hRequest.getSession(false) != null
                && errorContext.isError()) {
            hRequest.getRequestDispatcher("/WEB-INF/views/error.jsp")
                    .forward(request, response);
        }
    }

    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    @Override
    public void init(FilterConfig filterConfig) {
    }
}
