/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.aop;

import fi.ilmoeuro.balancomatic.web.error.TransactionException;
import fi.ilmoeuro.balancomatic.web.error.ValidationException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author user
 */
@WebFilter(
        urlPatterns = {"/*"},
        filterName = "transactionFilter")
public class TransactionFilter implements Filter {

    @Resource
    private UserTransaction utx;
    @Inject
    private EntityManager entityManager;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request,
            ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        try {
            utx.begin();
            entityManager.joinTransaction();
            chain.doFilter(request, response);
        } catch (ValidationException ex) {
            try {
                utx.rollback();
            } catch (Exception ex1) {
            }
            throw ex;
        } catch (IOException ex) {
            throw new TransactionException(ex);
        } catch (NotSupportedException ex) {
            throw new TransactionException(ex);
        } catch (ServletException ex) {
            throw new TransactionException(ex);
        } catch (SystemException ex) {
            throw new TransactionException(ex);
        } finally {
            try {
                if (utx.getStatus() == Status.STATUS_ACTIVE) {
                    utx.commit();
                }
            } catch (RollbackException rbe) {
                if (rbe.getCause() instanceof ConstraintViolationException) {
                    ConstraintViolationException cve
                            = (ConstraintViolationException) rbe.getCause();
                    throw new ValidationException(cve.getConstraintViolations());
                }
                throw new TransactionException(rbe);
            } catch (HeuristicMixedException ex) {
                if (ex.getCause() instanceof ConstraintViolationException) {
                    ConstraintViolationException cve
                            = (ConstraintViolationException) ex.getCause();
                    throw new ValidationException(cve.getConstraintViolations());
                }
                throw new TransactionException(ex);
            } catch (HeuristicRollbackException ex) {
                if (ex.getCause() instanceof ConstraintViolationException) {
                    ConstraintViolationException cve
                            = (ConstraintViolationException) ex.getCause();
                    throw new ValidationException(cve.getConstraintViolations());
                }
                throw new TransactionException(ex);
            } catch (SystemException ex) {
                Logger.getLogger(TransactionFilter.class.getName())
                        .log(Level.SEVERE, null, ex);
                throw new TransactionException(ex);
            } catch (SecurityException ex) {
                Logger.getLogger(TransactionFilter.class.getName())
                        .log(Level.SEVERE, null, ex);
                throw new TransactionException(ex);
            } catch (IllegalStateException ex) {
                Logger.getLogger(TransactionFilter.class.getName())
                        .log(Level.SEVERE, null, ex);
                throw new TransactionException(ex);
            }
        }
    }

    @Override
    public void destroy() {
    }
}
