/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.error;

import java.util.Set;
import javax.validation.ConstraintViolation;

/**
 *
 * @author Student
 */
public class ValidationException extends RuntimeException {

    private Set<ConstraintViolation<?>> violations;

    public ValidationException(Set<ConstraintViolation<?>> violations) {
        super("Constraint validation failed");
        this.violations = violations;
    }

    /**
     * @return the constraint violations
     */
    public Set<ConstraintViolation<?>> getViolations() {
        return violations;
    }

    /**
     * @param violations the constraint violations
     */
    public void setViolations(Set<ConstraintViolation<?>> violations) {
        this.violations = violations;
    }
}
