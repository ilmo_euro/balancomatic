/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.error;

/**
 *
 * @author user
 */
public class TransactionException extends RuntimeException {

    public TransactionException(Throwable cause) {
        super(cause);
    }

}
