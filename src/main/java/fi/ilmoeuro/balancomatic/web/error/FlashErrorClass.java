/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.error;

/**
 *
 * @author user
 */
public enum FlashErrorClass {

    ERROR("flashError"),
    INFO("flashInfo");
    private String cssClass;

    private FlashErrorClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public String getCssClass() {
        return cssClass;
    }
}
