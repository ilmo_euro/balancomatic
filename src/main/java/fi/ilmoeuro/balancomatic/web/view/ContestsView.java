/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.view;

import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.data.Resource;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import fi.ilmoeuro.balancomatic.repository.Contests;
import fi.ilmoeuro.balancomatic.repository.Projects;
import fi.ilmoeuro.balancomatic.repository.ResourceTypes;
import fi.ilmoeuro.balancomatic.repository.Resources;
import fi.ilmoeuro.balancomatic.web.context.CurrentEntitiesContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author user
 */
@Named("contests")
@RequestScoped
public class ContestsView {

    public static class ResourceTableCell {

        private final ResourceType resourceType;
        private final Resource resource;

        public ResourceTableCell(ResourceType resourceType,
                Resource resource) {
            this.resourceType = resourceType;
            this.resource = resource;
        }

        /**
         * @return the resourceType
         */
        public ResourceType getResourceType() {
            return resourceType;
        }

        /**
         * @return the value
         */
        public Resource getResource() {
            return resource;
        }
    }

    public static class ContestView {

        private final Contest contest;
        private final List<ResourceTableCell> resourceTableRow;

        public ContestView(Contest contest,
                List<ResourceTableCell> resourceTableRow) {
            this.contest = contest;
            this.resourceTableRow = resourceTableRow;
        }

        /**
         * @return the contest
         */
        public Contest getContest() {
            return contest;
        }

        /**
         * @return the resourceTableRow
         */
        public List<ResourceTableCell> getResourceTableRow() {
            return resourceTableRow;
        }
    }

    @Inject
    private Projects projects;
    @Inject
    private Contests contests;
    @Inject
    private CurrentEntitiesContext current;
    @Inject
    private Resources resources;
    @Inject
    private ResourceTypes resourceTypes;

    public ContestView getCurrent() {
        Contest contest = contests.findById(current.getContestId());
        Project project = projects.findById(current.getProjectId());
        return createContestView(contest, project);
    }

    public Collection<ContestView> getInCurrentProject() {
        Project project = projects.findById(current.getProjectId());
        List<ContestView> result = new ArrayList<>();
        for (Contest contest : contests.listByProject(project)) {
            ContestView contestView = createContestView(contest, project);
            result.add(contestView);
        }
        return result;
    }

    private ContestView createContestView(Contest contest, Project project) {
        Collection<Resource> myResources
                = resources.listByContest(contest);
        Collection<ResourceType> myResTypes
                = resourceTypes.listByProjectOrderByTypeId(project);
        List<ResourceTableCell> resourceTableRow = new ArrayList<>();
        outer:
        for (ResourceType resourceType : myResTypes) {
            for (Resource resource : myResources) {
                if (resource.getResourceType().equals(resourceType)) {
                    resourceTableRow.add(new ResourceTableCell(
                            resourceType, resource));
                    continue outer;
                }
            }
            resourceTableRow.add(new ResourceTableCell(resourceType, null));
        }
        ContestView contestView = new ContestView(contest, resourceTableRow);
        return contestView;
    }
}
