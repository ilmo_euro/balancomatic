/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.view;

import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import fi.ilmoeuro.balancomatic.web.context.CurrentEntitiesContext;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import static fi.ilmoeuro.balancomatic.config.ServletNames.*;

/**
 *
 * @author user
 */
@Named("url")
@RequestScoped
public class UrlView {

    @Inject
    private ViewContext viewContext;

    @Inject
    private CurrentEntitiesContext current;

    public String doLogin() {
        return viewContext.getPath() + "/" + LOGIN + "/";
    }

    public String doLogout() {
        return viewContext.getPath() + "/" + LOGOUT + "/";
    }

    public String projectShow() {
        return viewContext.getPath()
                + "/" + PROJECTS_SHOW + "/" + current.getProjectId();
    }

    public String projectShow(ProjectsView.ProjectView project) {
        return viewContext.getPath() + "/" + PROJECTS_SHOW + "/" + project.getId();
    }

    public String projectCreation() {
        return viewContext.getPath() + "/" + PROJECTS_CREATION + "/";
    }

    public String projectRename() {
        return projectShow() + "/" + PROJECTS_SHOW_RENAME + "/";
    }

    public String doProjectNew() {
        return viewContext.getPath() + "/" + PROJECTS_NEW + "/";
    }

    public String doProjectRename() {
        return viewContext.getPath() + "/" + PROJECTS_RENAME + "/";
    }

    public String doProjectDelete() {
        return viewContext.getPath() + "/" + PROJECTS_DELETE + "/";
    }

    public String resourceTypeList() {
        return viewContext.getPath() + "/" + RESOURCE_TYPES_LIST + "/"
                + current.getProjectId();
    }

    public String resourceTypeCreation() {
        return resourceTypeList() + "/addnew/";
    }

    public String resourceTypeModify(ResourceType rt) {
        return resourceTypeList() + "/modify/" + rt.getId() + "/";
    }

    public String doResourceTypeNew() {
        return viewContext.getPath() + "/" + RESOURCE_TYPES_NEW + "/";
    }

    public String doResourceTypeModify() {
        return viewContext.getPath() + "/" + RESOURCE_TYPES_MODIFY + "/";
    }

    public String doResourceTypeDelete() {
        return viewContext.getPath() + "/" + RESOURCE_TYPES_DELETE + "/";
    }

    public String contestList() {
        return viewContext.getPath() + "/" + CONTESTS_LIST + "/"
                + current.getProjectId();
    }

    public String contestCreation() {
        return contestList() + "/addnew/";
    }

    public String contestImport() {
        return contestList() + "/import/";
    }

    public String contestModify(Contest contest) {
        return contestList() + "/modify/" + contest.getId() + "/";
    }

    public String doContestImport() {
        return viewContext.getPath() + "/" + CONTESTS_IMPORT + "/";
    }

    public String doContestDelete() {
        return viewContext.getPath() + "/" + CONTESTS_DELETE + "/";
    }

    public String doContestNew() {
        return viewContext.getPath() + "/" + CONTESTS_NEW + "/";
    }

    public String doContestModify() {
        return viewContext.getPath() + "/" + CONTESTS_MODIFY + "/";
    }

    public String doCalculationTrigger() {
        return viewContext.getPath() + "/" + RESULTS_TRIGGER_CALCULATION + "/";
    }

    public String doCalculationDelete() {
        return viewContext.getPath() + "/" + RESULTS_DELETE_CALCULATION + "/";
    }

    public String analysis() {
        return "javascript:void(0)";
    }

    public String results() {
        return viewContext.getPath()
                + "/" + RESULTS_LIST + "/" + current.getProjectId();
    }
}
