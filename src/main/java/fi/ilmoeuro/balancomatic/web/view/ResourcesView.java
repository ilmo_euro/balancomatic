/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.view;

import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.data.Resource;
import fi.ilmoeuro.balancomatic.repository.Projects;
import fi.ilmoeuro.balancomatic.repository.Resources;
import fi.ilmoeuro.balancomatic.web.context.CurrentEntitiesContext;
import java.util.Collection;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author user
 */
@Named("resources")
@RequestScoped
public class ResourcesView {

    @Inject
    private Projects projects;
    @Inject
    private Resources resources;
    @Inject
    private CurrentEntitiesContext projectContext;

    public Collection<Resource> getCurrent() {
        Project project = projects.findById(projectContext.getProjectId());
        return resources.listByProject(project);
    }
}
