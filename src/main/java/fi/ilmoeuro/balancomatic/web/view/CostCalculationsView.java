/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.view;

import fi.ilmoeuro.balancomatic.data.CostCalculation;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.data.Resource;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import fi.ilmoeuro.balancomatic.data.ResourceTypeCostCalculation;
import fi.ilmoeuro.balancomatic.repository.CostCalculations;
import fi.ilmoeuro.balancomatic.repository.Projects;
import fi.ilmoeuro.balancomatic.repository.ResourceTypeCostCalculations;
import fi.ilmoeuro.balancomatic.repository.ResourceTypes;
import fi.ilmoeuro.balancomatic.repository.Resources;
import fi.ilmoeuro.balancomatic.web.context.CurrentEntitiesContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author user
 */
@Named("costCalculations")
@RequestScoped
public class CostCalculationsView {

    @Inject
    private Projects projects;
    @Inject
    private CostCalculations costCalculations;
    @Inject
    private ResourceTypeCostCalculations rtccs;
    @Inject
    private ResourceTypes rts;
    @Inject
    private CurrentEntitiesContext projectContext;

    public static class CostCalculationView {

        private final CostCalculation calculation;
        private final List<ResourceTypeCostCalculation> resCalcs;

        public CostCalculationView(CostCalculation cc,
                List<ResourceTypeCostCalculation> rc) {
            calculation = cc;
            resCalcs = rc;
        }

        /**
         * @return the calculation
         */
        public CostCalculation getCalculation() {
            return calculation;
        }

        /**
         * @return the cost calculations for each resource type
         */
        public List<ResourceTypeCostCalculation> getResCalcs() {
            return resCalcs;
        }
    }

    public Collection<CostCalculationView> getCurrent() {
        Project project = projects.findById(projectContext.getProjectId());
        Collection<CostCalculation> calcs
                = costCalculations.listByProject(project);
        List<CostCalculationView> result = new ArrayList<>();
        for (CostCalculation calc : calcs) {
            List<ResourceTypeCostCalculation> resCalcs
                    = new ArrayList<>();
            for (ResourceType rt : rts.listByProjectOrderByTypeId(project)) {
                resCalcs.add(
                        rtccs.findByResourceTypeAndCostCalculation(rt, calc)
                );
            }
            result.add(new CostCalculationView(calc, resCalcs));
        }
        return result;
    }
}
