/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.view;

import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.repository.Projects;
import fi.ilmoeuro.balancomatic.web.context.CurrentEntitiesContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author user
 */
@Named("projects")
@RequestScoped
public class ProjectsView {

    public static class ProjectView implements Comparable<ProjectView> {

        private Long id;
        private String name;
        private boolean current;

        public ProjectView(Long id, String name, boolean current) {
            this.id = id;
            this.name = name;
            this.current = current;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return true if this project is currently in view
         */
        public boolean isCurrent() {
            return current;
        }

        /**
         * @param current set as True to make this project currently in view
         */
        public void setCurrent(boolean current) {
            this.current = current;
        }

        @Override
        public int compareTo(ProjectView o) {
            return getId().compareTo(o.getId());
        }
    }
    @Inject
    private Projects projects;
    @Inject
    private CurrentEntitiesContext projectContext;

    private boolean isCurrent(Project entity) {
        return entity.getId() == projectContext.getProjectId();
    }

    public List<ProjectView> getAllProjects() {
        Collection<Project> entities = projects.listAll();
        List<ProjectView> result = new ArrayList<>();
        for (Project entity : entities) {
            result.add(new ProjectView(entity.getId(),
                    entity.getName(),
                    isCurrent(entity)));
        }
        Collections.sort(result);
        return result;
    }

    public ProjectView getCurrent() {
        Project project = projects.findById(projectContext.getProjectId());
        if (project != null) {
            return new ProjectView(project.getId(),
                    project.getName(),
                    isCurrent(project));
        } else {
            return null;
        }
    }
}
