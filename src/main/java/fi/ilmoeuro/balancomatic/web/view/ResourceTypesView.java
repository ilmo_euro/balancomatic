/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.view;

import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import fi.ilmoeuro.balancomatic.repository.Projects;
import fi.ilmoeuro.balancomatic.repository.ResourceTypes;
import fi.ilmoeuro.balancomatic.web.context.CurrentEntitiesContext;
import java.util.Collection;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author user
 */
@Named("resourceTypes")
@RequestScoped
public class ResourceTypesView {

    @Inject
    private Projects projects;
    @Inject
    private ResourceTypes resourceTypes;
    @Inject
    private CurrentEntitiesContext current;

    public Collection<ResourceType> getInCurrentProject() {
        Project project = projects.findById(current.getProjectId());
        return resourceTypes.listByProject(project);
    }

    public Collection<ResourceType> getInCurrentProjectOrderByTypeId() {
        Project project = projects.findById(current.getProjectId());
        return resourceTypes.listByProjectOrderByTypeId(project);
    }

    public ResourceType getCurrent() {
        return resourceTypes.findById(current.getResourceTypeId());
    }
}
