/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.controller;

import fi.ilmoeuro.balancomatic.config.ServletNames;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
@WebServlet("/" + ServletNames.PROJECTS_CREATION + "/*")
public class ProjectCreationController extends HttpServlet {

    @Inject
    private ViewContext viewContext;

    @Override
    public void doGet(HttpServletRequest req,
            HttpServletResponse resp)
            throws ServletException, IOException {
        viewContext.setView("projectcreation");
    }
}
