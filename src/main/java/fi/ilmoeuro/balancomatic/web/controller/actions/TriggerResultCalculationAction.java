/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.controller.actions;

import fi.ilmoeuro.balancomatic.config.ServletNames;
import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.data.Resource;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import fi.ilmoeuro.balancomatic.repository.Contests;
import fi.ilmoeuro.balancomatic.repository.CostCalculations;
import fi.ilmoeuro.balancomatic.repository.Projects;
import fi.ilmoeuro.balancomatic.repository.ResourceTypes;
import fi.ilmoeuro.balancomatic.repository.Resources;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

/**
 *
 * @author user
 */
@MultipartConfig
@WebServlet("/" + ServletNames.RESULTS_TRIGGER_CALCULATION + "/*")
public class TriggerResultCalculationAction extends Action {

    @Inject
    private CostCalculations costCalculations;

    @Inject
    private ViewContext viewContext;

    @Inject
    private Projects projects;

    @Override
    public void doPost(HttpServletRequest req,
            HttpServletResponse resp)
            throws ServletException, IOException {
        Project project
                = projects.findById(Long.valueOf(req.getParameter("projectId")));
        if (project != null) {
            costCalculations.triggerCalculation(project);
            viewContext.setRedirect(
                    viewContext.getPath() + "/"
                    + ServletNames.RESULTS_LIST
                    + "/" + project.getId());
        } else {
            throw new RuntimeException("Project not found"); // TODO: better ex
        }
    }
}
