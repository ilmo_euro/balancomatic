/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.controller.actions;

import fi.ilmoeuro.balancomatic.config.ServletNames;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import fi.ilmoeuro.balancomatic.repository.ResourceTypes;
import fi.ilmoeuro.balancomatic.web.context.CurrentEntitiesContext;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
@WebServlet("/" + ServletNames.RESOURCE_TYPES_DELETE + "/*")
public class DeleteResourceTypeAction extends Action {

    @Inject
    private ResourceTypes resourceTypes;
    @Inject
    private ViewContext viewContext;
    @Inject
    private CurrentEntitiesContext current;

    @Override
    public void doPost(HttpServletRequest req,
            HttpServletResponse resp)
            throws ServletException, IOException {
        ResourceType resourceType = resourceTypes.findById(Long.valueOf(
                req.getParameter("resourceTypeId")));
        Long projectId = resourceType.getProject().getId();
        if (resourceType != null) {
            resourceTypes.delete(resourceType);
        } else {
            throw new RuntimeException("Resource type not found"); // TODO: better ex
        }
        viewContext.setRedirect(viewContext.getPath() + "/resourcetypes/"
                + projectId + "/");
    }
}
