/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.controller.actions;

import fi.ilmoeuro.balancomatic.config.ServletNames;
import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.repository.Contests;
import fi.ilmoeuro.balancomatic.web.context.CurrentEntitiesContext;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
@WebServlet("/" + ServletNames.CONTESTS_DELETE + "/*")
public class DeleteContestAction extends Action {

    @Inject
    private Contests contests;
    @Inject
    private ViewContext viewContext;
    @Inject
    private CurrentEntitiesContext current;

    @Override
    public void doPost(HttpServletRequest req,
            HttpServletResponse resp)
            throws ServletException, IOException {
        Contest contest = contests.findById(Long.valueOf(
                req.getParameter("contestId")));
        Long projectId = current.getProjectId();
        if (contest != null) {
            contests.delete(contest);
        } else {
            throw new RuntimeException("Contest not found");
        }
        viewContext.setRedirect(viewContext.getPath() + "/contests/"
                + projectId + "/");
    }
}
