/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.controller.actions;

import fi.ilmoeuro.balancomatic.config.ServletNames;
import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.CostCalculation;
import fi.ilmoeuro.balancomatic.repository.Contests;
import fi.ilmoeuro.balancomatic.repository.CostCalculations;
import fi.ilmoeuro.balancomatic.web.context.CurrentEntitiesContext;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
@WebServlet("/" + ServletNames.RESULTS_DELETE_CALCULATION + "/*")
public class DeleteCalculationAction extends Action {

    @Inject
    private CostCalculations costCalculations;
    @Inject
    private ViewContext viewContext;
    @Inject
    private CurrentEntitiesContext current;

    @Override
    public void doPost(HttpServletRequest req,
            HttpServletResponse resp)
            throws ServletException, IOException {
        CostCalculation costCalculation = costCalculations.findById(Long.valueOf(
                req.getParameter("costCalculationId")));
        Long projectId = current.getProjectId();
        if (costCalculation != null) {
            costCalculations.delete(costCalculation);
        } else {
            throw new RuntimeException("Cost calculation not found");
        }
        viewContext.setRedirect(viewContext.getPath() + "/"
                + ServletNames.RESULTS_LIST + "/" + projectId + "/");
    }
}
