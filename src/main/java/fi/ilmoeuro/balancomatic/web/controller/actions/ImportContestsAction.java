/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.controller.actions;

import fi.ilmoeuro.balancomatic.config.ServletNames;
import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.data.Resource;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import fi.ilmoeuro.balancomatic.repository.Contests;
import fi.ilmoeuro.balancomatic.repository.Projects;
import fi.ilmoeuro.balancomatic.repository.ResourceTypes;
import fi.ilmoeuro.balancomatic.repository.Resources;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

/**
 *
 * @author user
 */
@MultipartConfig
@WebServlet("/" + ServletNames.CONTESTS_IMPORT + "/*")
public class ImportContestsAction extends Action {

    public static class ResourceDTO {

        public String typeId;
        public double value;
    }

    public static class ContestDTO {

        public boolean outcome;
        public List<ResourceDTO> resources;
    }

    @Inject
    private Projects projects;
    @Inject
    private ViewContext viewContext;
    @Inject
    private Contests contests;
    @Inject
    private Resources resources;
    @Inject
    private ResourceTypes resourceTypes;

    @Override
    public void doPost(HttpServletRequest req,
            HttpServletResponse resp)
            throws ServletException, IOException {
        Part part = req.getPart("file");
        InputStream is = part.getInputStream();
        Project project = projects.findById(
                Long.valueOf(req.getParameter("projectId")));

        Yaml yaml = new Yaml(new Constructor(ContestDTO.class));
        for (Object obj : yaml.loadAll(is)) {
            ContestDTO cdto = (ContestDTO) obj;
            Contest contest = contests.create();
            contest.setOutcome(cdto.outcome);
            for (ResourceDTO rdto : cdto.resources) {
                ResourceType rt = resourceTypes
                        .findOrCreateByProjectAndTypeId(project, rdto.typeId);
                Resource r = resources.create();
                r.setContest(contest);
                r.setResourceType(rt);
                r.setValue(rdto.value);
            }
        }

        viewContext.setRedirect(viewContext.getPath() + "/contests/"
                + project.getId() + "/");
    }
}
