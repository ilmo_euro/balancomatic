/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.controller.actions;

import fi.ilmoeuro.balancomatic.config.ServletNames;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.repository.Projects;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
@WebServlet("/" + ServletNames.PROJECTS_DELETE + "/*")
public class DeleteProjectAction extends Action {

    @Inject
    private Projects projects;
    @Inject
    private ViewContext viewContext;

    @Override
    public void doPost(HttpServletRequest req,
            HttpServletResponse resp)
            throws ServletException, IOException {
        Project project
                = projects.findById(Long.valueOf(req.getParameter("projectId")));
        if (project != null) {
            projects.delete(project);
        } else {
            throw new RuntimeException("Project not found"); // TODO: better ex
        }
        viewContext.setRedirect(viewContext.getPath() + "/");
    }
}
