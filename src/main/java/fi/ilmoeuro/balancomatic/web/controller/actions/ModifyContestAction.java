/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.controller.actions;

import fi.ilmoeuro.balancomatic.config.ServletNames;
import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.data.Resource;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import fi.ilmoeuro.balancomatic.repository.Contests;
import fi.ilmoeuro.balancomatic.repository.Projects;
import fi.ilmoeuro.balancomatic.repository.ResourceTypes;
import fi.ilmoeuro.balancomatic.repository.Resources;
import fi.ilmoeuro.balancomatic.web.context.CurrentEntitiesContext;
import fi.ilmoeuro.balancomatic.web.context.ErrorContext;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import java.io.IOException;
import java.util.Collection;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author user
 */
@WebServlet("/" + ServletNames.CONTESTS_MODIFY + "/*")
public class ModifyContestAction extends Action {

    @Inject
    private Projects projects;
    @Inject
    private Contests contests;
    @Inject
    private ResourceTypes resourceTypes;
    @Inject
    private Resources resources;
    @Inject
    private CurrentEntitiesContext current;
    @Inject
    private ViewContext viewContext;
    @Inject
    private ErrorContext errorContext;

    @Override
    public void doPost(HttpServletRequest req,
            HttpServletResponse resp)
            throws ServletException, IOException {
        long contestId = Long.valueOf(req.getParameter("contestId"));
        Contest c = contests.findById(contestId);
        Project p = projects.findByContest(c);
        Collection<ResourceType> rts = resourceTypes.listByProject(p);

        c.setOutcome(Boolean.valueOf(req.getParameter("outcome")));
        outer:
        for (ResourceType rt : rts) {
            String rtparam = req.getParameter("resource." + rt.getId());
            if (StringUtils.isNotEmpty(rtparam)) {
                Collection<Resource> rs = resources.listByContest(c);
                for (Resource r : rs) {
                    if (r.getResourceType().getId().equals(rt.getId())) {
                        r.setValue(Double.valueOf(rtparam));
                        continue outer;
                    }
                }
                // no resource found
                Resource resource = resources.create();
                resource.setContest(c);
                resource.setResourceType(rt);
                resource.setValue(Double.valueOf(rtparam));
            }
        }

        viewContext.setRedirect(viewContext.getPath() + "/contests/"
                + p.getId() + "/");
    }
}
