/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.controller.actions;

import fi.ilmoeuro.balancomatic.config.ConfigProperty;
import fi.ilmoeuro.balancomatic.config.ServletNames;
import fi.ilmoeuro.balancomatic.data.Account;
import fi.ilmoeuro.balancomatic.repository.Accounts;
import fi.ilmoeuro.balancomatic.web.context.AuthContext;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import fi.ilmoeuro.balancomatic.web.error.AuthenticationException;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
@WebServlet("/" + ServletNames.LOGIN + "/*")
public class LoginAction extends Action {

    @Inject
    private ViewContext viewContext;
    @Inject
    private AuthContext authContext;
    @Inject
    private Accounts accounts;
    @Inject
    @ConfigProperty("security.salt")
    private String salt;

    @Override
    public void doPost(HttpServletRequest req,
            HttpServletResponse resp)
            throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        Account account = accounts.findByName(username);

        if (account == null) {
            throw new AuthenticationException("Invalid credentials");
        } else if (account.passwordMatches(password, salt)) {
            authContext.setLoggedInAccount(account);
        } else {
            throw new AuthenticationException("Invalid credentials");
        }
        viewContext.setRedirect(
                viewContext.getPath() + "/");
    }
}
