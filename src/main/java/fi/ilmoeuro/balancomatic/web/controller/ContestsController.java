/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.controller;

import fi.ilmoeuro.balancomatic.config.ServletNames;
import fi.ilmoeuro.balancomatic.web.context.CurrentEntitiesContext;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import static fi.ilmoeuro.balancomatic.web.controller.ControllerUtils.getArg;
import java.io.IOException;
import java.net.HttpURLConnection;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Student
 */
@WebServlet("/" + ServletNames.CONTESTS_LIST + "/*")
public class ContestsController extends HttpServlet {

    @Inject
    private ViewContext viewContext;

    @Inject
    private CurrentEntitiesContext current;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            long projectId = Long.parseLong(getArg(req, resp, 0));
            viewContext.setView("contests");
            current.setProjectId(projectId);
        } catch (IndexOutOfBoundsException | IllegalArgumentException ex) {
            viewContext.setView(null);
            resp.sendError(HttpURLConnection.HTTP_NOT_FOUND);
        }
        try {
            String subView = getArg(req, resp, 1);
            viewContext.setSubView(subView);
            if ("modify".equals(subView)) {
                current.setContestId(Long.valueOf(getArg(req, resp, 2)));
            }
        } catch (IndexOutOfBoundsException ex) {
        }
    }
}
