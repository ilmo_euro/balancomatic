/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.controller.actions;

import fi.ilmoeuro.balancomatic.config.ServletNames;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import fi.ilmoeuro.balancomatic.repository.Projects;
import fi.ilmoeuro.balancomatic.repository.ResourceTypes;
import fi.ilmoeuro.balancomatic.web.context.ErrorContext;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
@WebServlet("/" + ServletNames.RESOURCE_TYPES_NEW + "/*")
public class NewResourceTypeAction extends Action {

    @Inject
    private Projects projects;
    @Inject
    private ResourceTypes resourceTypes;
    @Inject
    private ViewContext viewContext;
    @Inject
    private ErrorContext errorContext;

    @Override
    public void doPost(HttpServletRequest req,
            HttpServletResponse resp)
            throws ServletException, IOException {
        ResourceType rt = resourceTypes.create();
        rt.setTypeId(req.getParameter("typeId"));
        rt.setName(req.getParameter("name"));
        rt.setMinValue(Double.valueOf(req.getParameter("minValue")));
        rt.setMaxValue(Double.valueOf(req.getParameter("maxValue")));
        Project project = projects.findById(
                Long.valueOf(req.getParameter("projectId")));
        rt.setProject(project);
        viewContext.setRedirect(viewContext.getPath() + "/resourcetypes/"
                + project.getId() + "/");
    }
}
