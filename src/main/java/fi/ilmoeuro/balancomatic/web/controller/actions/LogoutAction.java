/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.controller.actions;

import fi.ilmoeuro.balancomatic.config.ServletNames;
import fi.ilmoeuro.balancomatic.web.context.AuthContext;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
@WebServlet("/" + ServletNames.LOGOUT + "/*")
public class LogoutAction extends Action {

    @Inject
    private ViewContext viewContext;
    @Inject
    private AuthContext authContext;

    @Override
    public void doPost(HttpServletRequest req,
            HttpServletResponse resp)
            throws ServletException, IOException {
        authContext.setLoggedInAccount(null);
        viewContext.setRedirect(
                viewContext.getPath() + "/loginform/");
    }
}
