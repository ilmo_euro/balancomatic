/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
public final class ControllerUtils {

    private ControllerUtils() {
    } // No instantiation allowed

    public static String[] getArgs(HttpServletRequest req,
            HttpServletResponse resp) {
        String[] args;
        try {
            args = req.getPathInfo().substring(1).split("/");
            if (args.length == 1 && "".equals(args[0])) {
                args = new String[0];
            }
            return args;
        } catch (NullPointerException npe) {
            // No slash at beginning
            return new String[0];
        }
    }

    public static String getArg(
            HttpServletRequest req,
            HttpServletResponse resp,
            int index)
            throws IndexOutOfBoundsException {
        return getArgs(req, resp)[index];
    }
}
