/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.web.controller;

import fi.ilmoeuro.balancomatic.config.ServletNames;
import fi.ilmoeuro.balancomatic.web.context.CurrentEntitiesContext;
import fi.ilmoeuro.balancomatic.web.context.ViewContext;
import static fi.ilmoeuro.balancomatic.web.controller.ControllerUtils.getArg;
import java.io.IOException;
import java.net.HttpURLConnection;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Student
 */
@WebServlet("/" + ServletNames.RESOURCE_TYPES_LIST + "/*")
public class ResourceTypesController extends HttpServlet {

    @Inject
    private ViewContext viewContext;

    @Inject
    private CurrentEntitiesContext current;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            long projectId = Long.parseLong(getArg(req, resp, 0));
            try {
                viewContext.setSubView(getArg(req, resp, 1));
                if ("modify".equals(viewContext.getSubView())) {
                    current.setResourceTypeId(Long.valueOf(getArg(req, resp, 2)));
                }
            } catch (IndexOutOfBoundsException ex) {
            }
            viewContext.setView("resourcetypes");
            current.setProjectId(projectId);
        } catch (IndexOutOfBoundsException | IllegalArgumentException ex) {
            viewContext.setView(null);
            resp.sendError(HttpURLConnection.HTTP_NOT_FOUND);
        }
        try {
            String subView = getArg(req, resp, 1);
            viewContext.setSubView(subView);
        } catch (Exception ex) {
        }
    }
}
