/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository;

import fi.ilmoeuro.balancomatic.data.Account;

/**
 *
 * @author Student
 */
public interface Accounts extends Repository<Account> {

    public Account findByName(String name);

}
