/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository.transient_;

import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.qualifier.Transient;
import fi.ilmoeuro.balancomatic.repository.Contests;
import fi.ilmoeuro.balancomatic.repository.Projects;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

/**
 *
 * @author Student
 */
@Dependent
@Alternative
@Transient
public class TransientProjects
        extends TransientRepository<Project>
        implements Projects {

    @Inject
    // @Transient -- add when implemented
    private Contests contests;

    @Override
    public Project create() {
        return super.create(Project.class);
    }

    @Override
    public Project findByContest(Contest contest) {
        for (Project project : listAll()) {
            if (contests.listByProject(project).contains(contest)) {
                return project;
            }
        }
        return null;
    }
}
