/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository.transient_;

import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.data.Resource;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import fi.ilmoeuro.balancomatic.qualifier.Transient;
import fi.ilmoeuro.balancomatic.repository.Resources;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Alternative;

/**
 *
 * @author Student
 */
@Dependent
@Alternative
@Transient
public class TransientResources
        extends TransientRepository<Resource>
        implements Resources {

    @Override
    public Collection<Resource> listByContest(Contest contest) {
        ArrayList<Resource> result = new ArrayList<>();
        for (Resource resource : entities.values()) {
            if (resource.getContest().getId() == contest.getId()) {
                result.add(resource);
            }
        }
        return Collections.unmodifiableCollection(result);
    }

    @Override
    public Collection<Resource> listByProject(Project project) {
        ArrayList<Resource> result = new ArrayList<>();
        for (Resource resource : entities.values()) {
            if (resource.getResourceType().getProject().getId()
                    == project.getId()) {
                result.add(resource);
            }
        }
        return Collections.unmodifiableCollection(result);
    }

    @Override
    public Resource create() {
        return super.create(Resource.class);
    }

    @Override
    public Resource findByContestAndType(Contest contest, ResourceType rt) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
