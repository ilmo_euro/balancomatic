/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository.transient_;

import fi.ilmoeuro.balancomatic.repository.Entity;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Student
 */
public abstract class TransientRepository<T extends Entity> {

    protected Map<Long, T> entities = new HashMap<>();

    public T findById(Long id) {
        return entities.get(id);
    }

    public Collection<T> listAll() {
        return Collections.unmodifiableCollection(entities.values());
    }

    protected T create(Class<T> clazz) {
        try {
            T t = clazz.newInstance();
            long i;

            for (i = 0; i < Long.MAX_VALUE; i++) {
                if (!entities.containsKey(i)) {
                    break;
                }
            }
            if (i == Long.MAX_VALUE) {
                throw new IllegalStateException("No space for entity");
            }

            t.setId(i);
            entities.put(i, t);
            return t;
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(TransientRepository.class.getName())
                    .log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void delete(T entity) {
        entities.remove(entity.getId());
    }
}
