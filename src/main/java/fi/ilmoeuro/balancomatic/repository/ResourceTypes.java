/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository;

import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import java.util.Collection;

/**
 *
 * @author Student
 */
public interface ResourceTypes extends Repository<ResourceType> {

    Collection<ResourceType> listByProject(Project project);

    Collection<ResourceType> listByProjectOrderByTypeId(Project project);

    Collection<ResourceType> listByContest(Contest contest);

    ResourceType findByProjectAndTypeId(Project project, String typeId);

    ResourceType findOrCreateByProjectAndTypeId(Project project,
            String typeId);
}
