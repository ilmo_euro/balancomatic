/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository;

import java.util.Collection;

/**
 *
 * @author Student
 */
public interface Repository<T> {

    T create();

    void delete(T resource);

    T findById(Long id);

    Collection<T> listAll();
}
