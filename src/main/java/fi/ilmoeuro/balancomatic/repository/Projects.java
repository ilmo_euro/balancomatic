/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository;

import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.Project;

/**
 *
 * @author Student
 */
public interface Projects extends Repository<Project> {

    public Project findByContest(Contest contest);

}
