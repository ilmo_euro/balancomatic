/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository;

import fi.ilmoeuro.balancomatic.data.CostCalculation;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import fi.ilmoeuro.balancomatic.data.ResourceTypeCostCalculation;

/**
 *
 * @author user
 */
public interface ResourceTypeCostCalculations extends
        Repository<ResourceTypeCostCalculation> {

    ResourceTypeCostCalculation findByResourceTypeAndCostCalculation(
            ResourceType resourceType, CostCalculation costCalculation);

}
