/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository.persistent;

import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.repository.Contests;
import java.util.Collection;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;

/**
 *
 * @author Student
 */
@ApplicationScoped
@Default
public class PersistentContests
        extends PersistentRepository<Contest>
        implements Contests {

    @Override
    public Contest create() {
        return create(Contest.class);
    }

    @Override
    public Collection<Contest> listAll() {
        return listAll(Contest.class);
    }

    @Override
    public Contest findById(Long id) {
        return findById(Contest.class, id);
    }

    @Override
    public Collection<Contest> listByProject(Project project) {
        return em.createNamedQuery("Contest.listByProjectId", Contest.class)
                .setParameter("projectId", project.getId())
                .getResultList();
    }

    @Override
    public void delete(Contest resource) {
        super.delete(resource);
    }
}
