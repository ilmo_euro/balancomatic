/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository.persistent;

import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import fi.ilmoeuro.balancomatic.repository.ResourceTypes;
import java.util.Collection;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;

/**
 *
 * @author user
 */
@ApplicationScoped
@Default
public class PersistentResourceTypes
        extends PersistentRepository<ResourceType>
        implements ResourceTypes {

    @Override
    public ResourceType create() {
        return super.create(ResourceType.class);
    }

    @Override
    public ResourceType findById(Long id) {
        return super.findById(ResourceType.class, id);
    }

    @Override
    public Collection<ResourceType> listAll() {
        return super.listAll(ResourceType.class);
    }

    @Override
    public void delete(ResourceType resourceType) {
        super.delete(resourceType);
    }

    @Override
    public Collection<ResourceType> listByProject(Project project) {
        return em.createNamedQuery("ResourceType.listByProjectId",
                ResourceType.class)
                .setParameter("projectId", project.getId())
                .getResultList();
    }

    @Override
    public Collection<ResourceType> listByContest(Contest contest) {
        return em.createNamedQuery("ResourceType.listByContestId",
                ResourceType.class)
                .setParameter("contestId", contest.getId())
                .getResultList();
    }

    @Override
    public ResourceType findByProjectAndTypeId(Project project, String typeId) {
        List<ResourceType> result
                = em.createNamedQuery("ResourceType.findByProjectIdAndTypeId",
                        ResourceType.class)
                .setParameter("projectId", project.getId())
                .setParameter("typeId", typeId)
                .getResultList();
        if (result.size() == 1) {
            return result.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Collection<ResourceType> listByProjectOrderByTypeId(Project project) {
        return em.createNamedQuery("ResourceType.listByProjectIdOrderByTypeId",
                ResourceType.class)
                .setParameter("projectId", project.getId())
                .getResultList();
    }

    @Override
    public ResourceType findOrCreateByProjectAndTypeId(Project project,
            String typeId) {
        ResourceType rt = findByProjectAndTypeId(project, typeId);
        if (rt == null) {
            rt = create();
            rt.setTypeId(typeId);
            rt.setMinValue(0.0);
            rt.setMaxValue(0.0);
            rt.setProject(project);
            rt.setName(typeId);
        }
        return rt;
    }
}
