/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository.persistent;

import Jama.Matrix;
import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.CostCalculation;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.data.Resource;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import fi.ilmoeuro.balancomatic.data.ResourceTypeCostCalculation;
import fi.ilmoeuro.balancomatic.repository.Contests;
import fi.ilmoeuro.balancomatic.repository.CostCalculations;
import fi.ilmoeuro.balancomatic.repository.ResourceTypeCostCalculations;
import fi.ilmoeuro.balancomatic.repository.ResourceTypes;
import fi.ilmoeuro.balancomatic.repository.Resources;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;

/**
 *
 * @author Student
 */
@ApplicationScoped
@Default
public class PersistentCostCalculations
        extends PersistentRepository<CostCalculation>
        implements CostCalculations {

    @Inject
    private ResourceTypes resourceTypes;

    @Inject
    private ResourceTypeCostCalculations rtccs;

    @Inject
    private Contests contests;

    @Inject
    private Resources resources;

    @Override
    public CostCalculation create() {
        CostCalculation result = super.create(CostCalculation.class);
        result.setDate(new Date());
        return result;
    }

    @Override
    public CostCalculation findById(Long id) {
        return super.findById(CostCalculation.class, id);
    }

    @Override
    public Collection<CostCalculation> listAll() {
        return super.listAll(CostCalculation.class);
    }

    @Override
    public void delete(CostCalculation entity) {
        super.delete(entity);
    }

    @Override
    public Collection<CostCalculation> listByProject(Project project) {
        return em.createNamedQuery("CostCalculation.listByProjectId",
                CostCalculation.class)
                .setParameter("projectId", project.getId())
                .getResultList();
    }

    @Override
    public void triggerCalculation(Project p) {
        Collection<ResourceType> rtList
                = resourceTypes.listByProjectOrderByTypeId(p);
        Collection<Contest> cList = contests.listByProject(p);

        Matrix solution = calculateSolution(cList, rtList);

        int i = 0;
        CostCalculation cc = create();
        cc.setNumContests(Long.valueOf(cList.size()));
        cc.setProject(p);
        for (ResourceType rt : rtList) {
            ResourceTypeCostCalculation rtcc = rtccs.create();
            rtcc.setResourceType(rt);
            rtcc.setSnapshot(cc);
            rtcc.setOptimalCost(solution.get(i, 0));
            i++;
        }
    }

    private Matrix calculateSolution(Collection<Contest> cList,
            Collection<ResourceType> rtList) {
        Matrix matA = new Matrix(cList.size(), rtList.size());
        Matrix matB = new Matrix(cList.size(), 1);
        int i = 0, j = 0;
        for (Contest c : cList) {
            i = 0;
            for (ResourceType rt : rtList) {
                Resource r = resources.findByContestAndType(c, rt);
                if (r != null) {
                    Double value = (r.getValue() - rt.getMinValue())
                            / (rt.getMaxValue() - rt.getMinValue());
                    matA.set(j, i, value);
                }
                i++;
            }
            matB.set(j, 0, c.getOutcome().booleanValue() ? 1 : 0);
            j++;
        }
        Matrix solution = matA.solve(matB);
        return solution;
    }
}
