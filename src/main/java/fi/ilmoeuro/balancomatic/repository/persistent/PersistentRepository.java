/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository.persistent;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 *
 * @author Student
 */
public abstract class PersistentRepository<T> {

    @Inject
    protected EntityManager em;

    protected T findById(Class<T> clazz, Long id) {
        try {
            String queryName = clazz.getSimpleName() + ".findById";
            return em.createNamedQuery(queryName, clazz)
                    .setParameter("id", id).getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    protected Collection<T> listAll(Class<T> clazz) {
        String queryName = clazz.getSimpleName() + ".listAll";
        return em.createNamedQuery(queryName, clazz)
                .getResultList();
    }

    protected T create(Class<T> clazz) {
        try {
            T t;
            t = clazz.newInstance();
            em.persist(t);
            return t;
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(PersistentResources.class.getName())
                    .log(Level.SEVERE, null, ex);
            return null;
        }
    }

    protected void delete(T entity) {
        em.remove(entity);
    }
}
