/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository.persistent;

import fi.ilmoeuro.balancomatic.data.CostCalculation;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import fi.ilmoeuro.balancomatic.data.ResourceTypeCostCalculation;
import fi.ilmoeuro.balancomatic.repository.ResourceTypeCostCalculations;
import java.util.Collection;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;

/**
 *
 * @author Student
 */
@ApplicationScoped
@Default
public class PersistentResourceTypeCostCalculations
        extends PersistentRepository<ResourceTypeCostCalculation>
        implements ResourceTypeCostCalculations {

    @Override
    public ResourceTypeCostCalculation create() {
        return super.create(ResourceTypeCostCalculation.class);
    }

    @Override
    public ResourceTypeCostCalculation findById(Long id) {
        return super.findById(ResourceTypeCostCalculation.class, id);
    }

    @Override
    public Collection<ResourceTypeCostCalculation> listAll() {
        return super.listAll(ResourceTypeCostCalculation.class);
    }

    @Override
    public void delete(ResourceTypeCostCalculation entity) {
        super.delete(entity);
    }

    public ResourceTypeCostCalculation findByResourceTypeAndCostCalculation(
            ResourceType resourceType,
            CostCalculation costCalculation
    ) {
        List<ResourceTypeCostCalculation> result
                = em.createNamedQuery(
                        "ResourceTypeCostCalculation."
                        + "findByResourceTypeIdAndCostCalculationId",
                        ResourceTypeCostCalculation.class)
                .setParameter("resourceTypeId", resourceType.getId())
                .setParameter("costCalculationId", costCalculation.getId())
                .getResultList();
        if (result.size() == 1) {
            return result.get(0);
        } else {
            return null;
        }
    }
}
