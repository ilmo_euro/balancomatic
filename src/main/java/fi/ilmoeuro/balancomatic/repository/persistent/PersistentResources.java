/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository.persistent;

import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.data.Resource;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import fi.ilmoeuro.balancomatic.repository.Resources;
import java.util.Collection;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;

/**
 *
 * @author Student
 */
@ApplicationScoped
@Default
public class PersistentResources
        extends PersistentRepository<Resource>
        implements Resources {

    @Override
    public Resource create() {
        return create(Resource.class);
    }

    @Override
    public Collection<Resource> listAll() {
        return listAll(Resource.class);
    }

    @Override
    public Resource findById(Long id) {
        return findById(Resource.class, id);
    }

    @Override
    public Collection<Resource> listByContest(Contest contest) {
        return em.createNamedQuery("Resource.listByContestId", Resource.class)
                .setParameter("contestId", contest.getId())
                .getResultList();
    }

    @Override
    public Collection<Resource> listByProject(Project project) {
        return em.createNamedQuery("Resource.listByProjectId", Resource.class)
                .setParameter("projectId", project.getId())
                .getResultList();
    }

    @Override
    public void delete(Resource resource) {
        super.delete(resource);
    }

    @Override
    public Resource findByContestAndType(Contest contest, ResourceType rt) {
        List<Resource> result
                = em.createNamedQuery(
                        "Resource.findByContestIdAndResourceTypeId",
                        Resource.class)
                .setParameter("contestId", contest.getId())
                .setParameter("resourceTypeId", rt.getId())
                .getResultList();
        if (result.size() == 1) {
            return result.get(0);
        } else {
            return null;
        }
    }
}
