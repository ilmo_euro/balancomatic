/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository.persistent;

import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.repository.Projects;
import java.util.Collection;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;

/**
 *
 * @author Student
 */
@ApplicationScoped
@Default
public class PersistentProjects
        extends PersistentRepository<Project>
        implements Projects {

    @Override
    public Project create() {
        return super.create(Project.class);
    }

    @Override
    public Project findById(Long id) {
        return super.findById(Project.class, id);
    }

    @Override
    public Collection<Project> listAll() {
        return super.listAll(Project.class);
    }

    @Override
    public void delete(Project entity) {
        super.delete(entity);
    }

    @Override
    public Project findByContest(Contest contest) {
        List<Project> result
                = em.createNamedQuery("Project.findByContestId", Project.class)
                .setParameter("contestId", contest.getId())
                .getResultList();
        if (result.size() == 1) {
            return result.get(0);
        } else {
            return null;
        }
    }
}
