/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository.persistent;

import fi.ilmoeuro.balancomatic.data.Account;
import fi.ilmoeuro.balancomatic.repository.Accounts;
import java.util.Collection;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;

/**
 *
 * @author Student
 */
@ApplicationScoped
@Default
public class PersistentAccounts
        extends PersistentRepository<Account>
        implements Accounts {

    @Override
    public Account create() {
        return super.create(Account.class);
    }

    @Override
    public Account findById(Long id) {
        return super.findById(Account.class, id);
    }

    @Override
    public Collection<Account> listAll() {
        return super.listAll(Account.class);
    }

    @Override
    public void delete(Account entity) {
        super.delete(entity);
    }

    @Override
    public Account findByName(String name) {
        List<Account> result
                = em.createNamedQuery("Account.findByName", Account.class)
                .setParameter("name", name)
                .getResultList();
        if (result.size() == 1) {
            return result.get(0);
        } else {
            return null;
        }
    }
}
