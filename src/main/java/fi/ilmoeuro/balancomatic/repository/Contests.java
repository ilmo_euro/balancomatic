/* * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository;

import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.Project;
import java.util.Collection;

/**
 *
 * @author user
 */
public interface Contests extends Repository<Contest> {

    Collection<Contest> listByProject(Project project);
}
