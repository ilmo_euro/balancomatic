/* * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository;

import fi.ilmoeuro.balancomatic.data.Contest;
import fi.ilmoeuro.balancomatic.data.Project;
import fi.ilmoeuro.balancomatic.data.Resource;
import fi.ilmoeuro.balancomatic.data.ResourceType;
import java.util.Collection;

/**
 *
 * @author user
 */
public interface Resources extends Repository<Resource> {

    Collection<Resource> listByContest(Contest contest);

    Collection<Resource> listByProject(Project project);

    Resource findByContestAndType(Contest contest, ResourceType rt);
}
