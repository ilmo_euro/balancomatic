/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.repository;

import fi.ilmoeuro.balancomatic.data.CostCalculation;
import fi.ilmoeuro.balancomatic.data.Project;
import java.util.Collection;

/**
 *
 * @author user
 */
public interface CostCalculations extends Repository<CostCalculation> {

    public void triggerCalculation(Project p);

    public Collection<CostCalculation> listByProject(Project project);

}
