/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.config;

/**
 *
 * @author user
 */
public final class ServletNames {

    private ServletNames() {
    }

    public static final String CONTESTS_NEW = "newcontest";
    public static final String CONTESTS_IMPORT = "importcontests";
    public static final String CONTESTS_LIST = "contests";
    public static final String CONTESTS_MODIFY = "modifycontest";
    public static final String CONTESTS_DELETE = "deletecontest";
    public static final String LOGIN_FORM = "loginform";
    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";
    public static final String PROJECTS_NEW = "newproject";
    public static final String PROJECTS_CREATION = "projectcreation";
    public static final String PROJECTS_SHOW = "overview";
    public static final String PROJECTS_SHOW_RENAME = "rename";
    public static final String PROJECTS_RENAME = "renameproject";
    public static final String PROJECTS_DELETE = "deleteproject";
    public static final String RESOURCE_TYPES_NEW = "newresourcetype";
    public static final String RESOURCE_TYPES_LIST = "resourcetypes";
    public static final String RESOURCE_TYPES_MODIFY = "modifyresourcetype";
    public static final String RESOURCE_TYPES_DELETE = "deleteresourcetype";
    public static final String RESULTS_LIST = "results";
    public static final String RESULTS_TRIGGER_CALCULATION = "triggercalculation";
    public static final String RESULTS_DELETE_CALCULATION = "deletecalculation";

}
