/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.ilmoeuro.balancomatic.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

/**
 *
 * @author user
 */
@Singleton
@Startup
public class ConfigProvider {

    private final Properties properties = new Properties();
    private static final String PROPERTIES_FILE
            = "/fi/ilmoeuro/balancomatic/config.properties";
    private static final String PROPERTIES_SYSTEM_PROPERTY
            = "balancomatic.config";

    @PostConstruct
    public void initialize() {
        merge(loadDefaultProperties());
        merge(loadApplicationProperties());
    }

    private Properties loadDefaultProperties() {
        Properties result = new Properties();
        try (InputStream in = getClass().getResourceAsStream(PROPERTIES_FILE)) {
            result.load(in);
        } catch (IOException e) {
            Logger.getLogger(ConfigProvider.class.getName()).log(Level.SEVERE,
                    e.getMessage());
        }
        return result;
    }

    private Properties loadApplicationProperties() {
        String fileName = System.getProperty(PROPERTIES_SYSTEM_PROPERTY);
        Properties result = new Properties();
        if (fileName != null) {
            try (InputStream in = getClass()
                    .getResourceAsStream(fileName)) {
                result.load(in);
            } catch (IOException e) {
                Logger.getLogger(ConfigProvider.class.getName())
                        .log(Level.SEVERE, e.getMessage());
            }
        }
        return result;
    }

    public void merge(Properties target) {
        synchronized (properties) {
            properties.putAll(target);
        }
    }

    @Produces
    @ConfigProperty("")
    public String getString(InjectionPoint point) {
        String property = point.getAnnotated()
                .getAnnotation(ConfigProperty.class).value();
        String valueForFieldName = properties.getProperty(property);
        return (valueForFieldName == null) ? "" : valueForFieldName;
    }

}
