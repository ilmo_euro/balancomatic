<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<t:masterpage>
    <jsp:attribute name="title">Add new project</jsp:attribute>
    <jsp:body>
        <h1>Add new project</h1>
        <form
            method="post"
            accept-charset="UTF-8"
            action="${url.doProjectNew()}">
            <dl>
                <dt>Project name</dt>
                <dd><input type="text" name="name" /></dd>
                <dt>&nbsp;</dt>
                <dd><button type="submit">Create project</button></dd>
            </dl>
        </form>
    </jsp:body>
</t:masterpage>
