<%--
    Document   : index
    Created on : Sep 18, 2013, 3:08:32 PM
    Author     : Student
--%>

<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="t" %>
<t:projectpage>
    <jsp:attribute name="title">Balancomatic</jsp:attribute>
    <jsp:body>
        <dl id="actions">
            <dt>General</dt>
            <dd><form method="post"
                      action="${url.doCalculationTrigger()}">
                    <input type="hidden"
                           name="projectId"
                           value="${projects.current.id}" />
                    <button type="submit">Trigger calculation</button>
                </form></dd>
        </dl>
        <div id="content">
            <h1>${projects.current.name} - Calculations</h1>
            <table>
                <thead>
                    <tr>
                        <th colspan="3">&nbsp</th>
                        <th colspan="${resourceTypes.inCurrentProject.size()}">
                            Optimal costs</th>
                    <tr>
                        <th>Calculation</th>
                        <th>Date</th>
                        <th>Number of contests</th>
                            <c:forEach var="restype"
                                       items="${resourceTypes
                                                .inCurrentProjectOrderByTypeId}">
                            <th>${restype.typeId}</th>
                            </c:forEach>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="costCalculation"
                               items="${costCalculations.current}">
                        <tr>
                            <td>${costCalculation.calculation.id}</td>
                            <td>${costCalculation.calculation.date}</td>
                            <td>${costCalculation.calculation.numContests}</td>
                            <c:forEach var="cell"
                                       items="${costCalculation.resCalcs}">
                                <c:if test="${not empty cell}">
                                    <td><fmt:formatNumber value="${cell.optimalCost}"
                                                      maxFractionDigits="2" />
                                    </td>
                                </c:if>
                                <c:if test="${empty cell}">
                                    <td>&nbsp;</td>
                                </c:if>
                            </c:forEach>
                            <td class="narrow">
                                <form method="post"
                                      action="${url.doCalculationDelete()}">
                                    <input type="hidden"
                                           name="costCalculationId"
                                           value="${costCalculation.calculation.id}" />
                                    <button type="submit">Delete</button>
                                </form></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </jsp:body>
</t:projectpage>