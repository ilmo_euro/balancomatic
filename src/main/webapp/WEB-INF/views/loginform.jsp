<%--
    Document   : loginform
    Created on : Dec 20, 2013, 9:52:51 AM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Please login to continue</title>
        <meta charset="utf-8" />
        <%-- TODO: Change to CSS on production use --%>
        <link rel="stylesheet/less"
              href="${context.path}/static/less/reset.less" />
        <link rel="stylesheet/less"
              href="${context.path}/static/less/master.less" />
        <link rel="stylesheet/less"
              href="${context.path}/static/less/loginform.less" />
        <%-- TODO: Remove on production use --%>
        <script src="${context.path}/static/js/less.js"
        type="text/javascript"></script>
    </head>
    <body>
        <div class="login-box">
            <h1>Login</h1>
            <form method="post" action="${url.doLogin()}">
                <div>
                    <label for="username">Username:</label>
                    <input type="text" name="username" />
                </div>
                <div>
                    <label for="password">Password:</label>
                    <input type="password" name="password" />
                </div>
                <div class="login-button-box">
                    <button type="submit">Login</button>
                </div>
            </form>
        </div>
    </body>
</html>
