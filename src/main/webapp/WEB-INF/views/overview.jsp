<%--
    Document   : index
    Created on : Sep 18, 2013, 3:08:32 PM
    Author     : Student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="t" %>
<t:projectpage>
    <jsp:attribute name="title">Balancomatic</jsp:attribute>
    <jsp:body>
        <dl id="actions">
            <dt>General</dt>
            <dd><form method="post" action="${url.doProjectDelete()}">
                    <input type="hidden" name="projectId"
                           value="${current.projectId}" />
                    <button type="submit">Delete project</button>
                </form></dd>
            <dd><form method="get"
                      action="${url.projectRename()}">
                    <button type="submit">Rename project...</button>
                </form></dd>
        </dl>
        <div id="content">
            <c:if test="${empty context.subView}">
                <h1>${projects.current.name}</h1>
                <dl>
                    <dt>Number of resource types</dt>
                    <dd>${fn:length(resourceTypes.inCurrentProject)}</dd>
                    <dt>Number of resources</dt>
                    <dd>${fn:length(resources.current)}</dd>
                </dl>
            </c:if>
            <c:if test='${context.subView eq "rename"}'>
                <form method="post" action="${url.doProjectRename()}">
                    <dl>
                        <dt>New name</dt>
                        <dd>
                            <input type="text" name="newName" />
                        </dd>
                        <input type="hidden" name="projectId"
                               value="${current.projectId}" />
                        <dd>
                            <button type="submit">Rename project</button>
                        </dd>
                    </dl>
                </form>
            </c:if>
        </div>
    </jsp:body>
</t:projectpage>