<%--
    Document   : error
    Created on : Sep 18, 2013, 3:08:32 PM
    Author     : Student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="t" %>
<t:masterpage>
    <jsp:attribute name="title">Balancomatic</jsp:attribute>
    <jsp:body>
        <h1>Error!</h1>
        <p class="errorMessage">${errorContext.errorMessage}</p>
        <ul class="errorDetails">
            <c:forEach items="${errorContext.details}"
                       var="detail">
                <li>${detail}</li>
                </c:forEach>
        </ul>
    </jsp:body>
</t:masterpage>