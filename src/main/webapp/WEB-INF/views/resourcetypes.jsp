<%--
    Document   : index
    Created on : Sep 18, 2013, 3:08:32 PM
    Author     : Student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="t" %>
<t:projectpage>
    <jsp:attribute name="title">Balancomatic</jsp:attribute>
    <jsp:body>
        <dl id="actions">
            <dt>General</dt>
            <dd><form method="get"
                      action="${url.resourceTypeList()}">
                    <button type="submit">List resource types</button>
                </form></dd>
            <dd><form method="get"
                      action="${url.resourceTypeCreation()}">
                    <button type="submit">Add new resource type</button>
                </form></dd>
        </dl>
        <div id="content">
            <c:if test="${empty context.subView}">
                <h1>${projects.current.name} - Resource types</h1>
                <table>
                    <tbody>
                        <tr>
                            <th>TypeID</th>
                            <th>Name</th>
                            <th>Minimum value</th>
                            <th>Maximum value</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                        <c:forEach items="${resourceTypes.inCurrentProject}"
                                   var="resourceType">
                            <tr>
                                <td>${resourceType.typeId}</td>
                                <td>${resourceType.name}</td>
                                <td>${resourceType.minValue}</td>
                                <td>${resourceType.maxValue}</td>
                                <td class="narrow">
                                    <form method="get"
                                          action="${url.resourceTypeModify(
                                                    resourceType)}">
                                        <button type="submit">Modify</button>
                                    </form>
                                </td>
                                <td class="narrow">
                                    <form method="post"
                                          action="${url.doResourceTypeDelete()}">
                                        <input type="hidden"
                                               name="resourceTypeId"
                                               value="${resourceType.id}" />
                                        <button type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <c:if test="${context.subView eq 'addnew'}">
                <h1>${projects.current.name} - New resource type</h1>
                <form
                    method="post"
                    accept-charset="UTF-8"
                    action="${url.doResourceTypeNew()}">
                    <input type="hidden"
                           name="projectId"
                           value="${current.projectId}" />
                    <dl>
                        <dt>TypeID</dt>
                        <dd><input type="text" name="typeId" /></dd>
                        <dt>Name</dt>
                        <dd><input type="text" name="name" /></dd>
                        <dt>Minimum value</dt>
                        <dd><input type="text" name="minValue"/></dd>
                        <dt>Maximum value</dt>
                        <dd><input type="text" name="maxValue"/></dd>
                        <dt>&nbsp;</dt>
                        <dd>
                            <button type="submit">Create resource type</button>
                        </dd>
                    </dl>
                </form>
            </c:if>
            <c:if test="${context.subView eq 'modify'}">
                <h1>${projects.current.name} - Modify resource type</h1>
                <form
                    method="post"
                    accept-charset="UTF-8"
                    action="${url.doResourceTypeModify()}">
                    <input type="hidden"
                           name="resourceTypeId"
                           value="${current.resourceTypeId}" />
                    <dl>
                        <dt>TypeID</dt>
                        <dd><input type="text"
                                   name="typeId"
                                   value="${resourceTypes.current.typeId}"/>
                        </dd>
                        <dt>Name</dt>
                        <dd><input type="text"
                                   name="name"
                                   value="${resourceTypes.current.name}"/>
                        </dd>
                        <dt>Minimum value</dt>
                        <dd><input type="text"
                                   name="minValue"
                                   value="${resourceTypes.current.minValue}"/>
                        </dd>
                        <dt>Maximum value</dt>
                        <dd><input type="text"
                                   name="maxValue"
                                   value="${resourceTypes.current.maxValue}"/>
                        </dd>
                        <dt>&nbsp;</dt>
                        <dd>
                            <button type="submit">Save</button>
                        </dd>
                    </dl>
                </form>
            </c:if>
        </div>
    </jsp:body>
</t:projectpage>