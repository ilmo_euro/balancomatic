<%--
    Document   : index
    Created on : Sep 18, 2013, 3:08:32 PM
    Author     : Student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="t" %>
<t:projectpage>
    <jsp:attribute name="title">Balancomatic</jsp:attribute>
    <jsp:body>
        <dl id="actions">
            <dt>General</dt>
            <dd><form method="get"
                      action="${url.contestList()}">
                    <button type="submit">List contests</button>
                </form></dd>
            <dd><form method="get"
                      action="${url.contestCreation()}">
                    <button type="submit">Create new contest</button>
                </form></dd>
            <dd><form method="get"
                      action="${url.contestImport()}">
                    <button type="submit">Import contests</button>
                </form></dd>
        </dl>
        <div id="content">
            <c:if test="${empty context.subView}">
                <h1>${projects.current.name} - Contests</h1>
                <table>
                    <thead>
                        <tr>
                            <th>Outcome</th>
                                <c:forEach var="restype"
                                           items="${resourceTypes
                                                    .inCurrentProjectOrderByTypeId}">
                                <th>${restype.typeId}</th>
                                </c:forEach>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="contest"
                                   items="${contests.inCurrentProject}">
                            <tr>
                                <td>${contest.contest.outcome ?
                                      'Victory' : 'Defeat/Draw'}</td>
                                    <c:forEach var="cell"
                                               items="${contest.resourceTableRow}">
                                        <c:if test="${not empty cell.resource}">
                                        <td>${cell.resource.value}</td>
                                    </c:if>
                                    <c:if test="${empty cell.resource}">
                                        <td>&nbsp;</td>
                                    </c:if>
                                </c:forEach>
                                <td class="narrow">
                                    <form method="get"
                                          action="${url.contestModify(contest.contest)}">
                                        <button type="submit">Modify</button>
                                    </form></td>
                                <td class="narrow">
                                    <form method="post"
                                          action="${url.doContestDelete()}">
                                        <input type="hidden"
                                               name="contestId"
                                               value="${contest.contest.id}" />
                                        <button type="submit">Delete</button>
                                    </form></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <c:if test="${context.subView eq 'import'}">
                <h1>${projects.current.name} - Import contests</h1>
                <form
                    enctype="multipart/form-data"
                    method="post"
                    accept-charset="UTF-8"
                    action="${url.doContestImport()}">
                    <input type="hidden"
                           name="projectId"
                           value="${current.projectId}" />
                    <dl>
                        <dt>File</dt>
                        <dd>Select a YAML file</dd>
                        <dd>&nbsp;</dd>
                        <dd><input type="file" name="file" /></dd>
                        <dt>&nbsp;</dt>
                        <button type="submit">Upload file</button>
                        </dd>
                    </dl>
                </form>
            </c:if>
            <c:if test="${context.subView eq 'addnew'}">
                <h1>${projects.current.name} - New contest</h1>
                <form
                    method="post"
                    accept-charset="UTF-8"
                    action="${url.doContestNew()}">
                    <input type="hidden"
                           name="projectId"
                           value="${current.projectId}" />
                    <dl>
                        <dt>Outcome</dt>
                        <dd><select name="outcome">
                                <option value="true">Victory</option>
                                <option value="false">Defeat/Draw</option>
                            </select></dd>
                            <c:forEach items="${resourceTypes.inCurrentProject}"
                                       var="resourceType">
                            <dt>${resourceType.name}</dt>
                            <dd><input type="text"
                                       name="resource.${resourceType.id}" />
                            </dd>
                        </c:forEach>
                        <dt>&nbsp;</dt>
                        <dd>
                            <button type="submit">Create contest</button>
                        </dd>
                    </dl>
                </form>
            </c:if>
            <c:if test="${context.subView eq 'modify'}">
                <%-- simple alias --%>
                <c:set var="curr" value="${contests.current}" />
                <h1>${projects.current.name} - Modify contest</h1>
                <form
                    method="post"
                    accept-charset="UTF-8"
                    action="${url.doContestModify()}">
                    <input type="hidden"
                           name="contestId"
                           value="${curr.contest.id}">
                    <dl>
                        <dt>Outcome</dt>
                        <dd><select name="outcome">
                                <c:if test="${curr.contest.outcome}">
                                    <option value="true" selected="selected">
                                        Victory</option>
                                    <option value="false">
                                        Defeat/Draw</option>
                                    </c:if>
                                    <c:if test="${!curr.contest.outcome}">
                                    <option value="true">
                                        Victory</option>
                                    <option value="false" selected="selected">
                                        Defeat/Draw</option>
                                    </c:if>
                            </select></dd>
                            <c:forEach items="${curr.resourceTableRow}"
                                       var="cell">
                            <dt>${cell.resourceType.name}</dt>
                            <c:if test="${not empty cell.resource}">
                                <dd><input type="text"
                                           name="resource.${cell.resourceType.id}"
                                           value="${cell.resource.value}" />
                                </dd>
                            </c:if>
                            <c:if test="${empty cell.resource}">
                                <dd><input type="text"
                                           name="resource.${cell.resourceType.id}" />
                                </dd>
                            </c:if>
                        </c:forEach>
                        <dt>&nbsp;</dt>
                        <dd>
                            <button type="submit">Modify contest</button>
                        </dd>
                    </dl>
                </form>
            </c:if>
        </div>
    </jsp:body>
</t:projectpage>