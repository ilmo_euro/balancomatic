<%@tag description="Master page, template for other pages" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@attribute name="title"%>
<%@attribute name="css" fragment="true"%>
<%@attribute name="js" fragment="true"%>
<%@attribute name="nav" fragment="true"%>

<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8" />
        <%-- TODO: Change to CSS on production use --%>
        <link rel="stylesheet/less"
              href="${context.path}/static/less/reset.less" />
        <link rel="stylesheet/less"
              href="${context.path}/static/less/master.less" />
        <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow'
              rel='stylesheet' type='text/css'>
        <%-- TODO: Remove on production use --%>
        <script src="${context.path}/static/js/less.js"
        type="text/javascript"></script>
        <jsp:invoke fragment="css" />
    </head>
    <body>
        <div id="pageHeader">
            <div id="logout">
                <div>
                    <form method="post" action="${url.doLogout()}">
                        <button type="submit">Logout</button>
                    </form>
                </div>
            </div>
            <div id="projectNav">
                <div>
                    <%-- Project navigation --%>
                    <c:forEach items="${projects.allProjects}" var="project">
                        <c:if test="${project.current}">
                            <span class="current">${project.name}</span>
                        </c:if>
                        <c:if test="${not project.current}">
                            <a class="notcurrent"
                               href="${url.projectShow(project)}"
                               >${project.name}</a>
                        </c:if>
                    </c:forEach>
                    <a
                        class="${context.view eq 'projectcreation'
                                 ? 'current'
                                 : 'notcurrent'} plus"
                        href="${url.projectCreation()}"
                        >+</a>
                </div>
            </div>
            <div id="localNav">
                <div>
                    <%-- Local navigation --%>
                    <jsp:invoke fragment="nav" />
                </div>
            </div>
        </div>
        <div id="pageContent">
            <div>
                <jsp:doBody />
            </div>
            <jsp:invoke fragment="js" />
        </div>
    </body>
</html>