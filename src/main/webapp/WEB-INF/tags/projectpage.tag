<%@tag description="Template for project pages" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<%@attribute name="title"%>
<%@attribute name="css" fragment="true"%>
<%@attribute name="js" fragment="true"%>
<%@attribute name="nav" fragment="true"%>

<t:masterpage>
    <jsp:attribute name="title">${title}</jsp:attribute>
    <jsp:attribute name="css">
        <jsp:invoke fragment="css" />
    </jsp:attribute>
    <jsp:attribute name="js">
        <jsp:invoke fragment="js" />
    </jsp:attribute>
    <jsp:attribute name="nav">
        <a
            href="${url.projectShow()}"
            >Overview</a>
        <a
            href="${url.resourceTypeList()}"
            >Resource types</a>
        <a
            href="${url.contestList()}"
            >Contests</a>
        <a
            href="${url.analysis()}"
            >Analysis</a>
        <a
            href="${url.results()}"
            >Results</a>
        <jsp:invoke fragment="nav" />
    </jsp:attribute>
    <jsp:body>
        <jsp:doBody />
    </jsp:body>
</t:masterpage>